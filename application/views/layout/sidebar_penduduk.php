<section class="blog-wrap" style="background: #f7f7f7 !important; padding-top: 30px;">
    <div class="container">
      <div class="row">
          <div class="col-lg-4 col-12 d-flex align-items-center justify-content-between justify-content-lg-start">
                <?php
                    $id = $this->session->userdata('id');
                    $data = $this->db->query("SELECT * FROM tbl_penduduk WHERE id='$id' LIMIT 1")->row_array(); 
                ?>
              <div class="d-flex align-items-center">
                  <div class="box-image">
                        <?php if($this->session->userdata('foto') == ""){ ?> 
                            <img src="<?= base_url('theme/images/not_image.png') ?>"  class="rounded-circle img-fluid" alt="">
                        <?php }else{ ?> 
                            <img src="<?= base_url('assets/images/penduduk/') ?><?= $data['foto']; ?>"  class="rounded-circle img-fluid" alt="">
                        <?php } ?>
                  </div>
                  <div class="desc-profile ml-3">
                      <p class="p-muted m-0">
                          Selamat Datang,
                      </p>
                      <h4>
                        <?= $data['nama']; ?>
                      </h4>
                  </div>
              </div>
              <div class="dropdown ml-3">
                  <a class="text-muted font-weight-bold h4-custom nav-link" href="#" role="button" id="dropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fa fa-ellipsis-v"></i>
                  </a>
                  <ul class="dropdown-menu dropdown-menu-right">
                    <li>
                        <a class="dropdown-item" href="<?= base_url('penduduk/profil') ?>">Edit Profil</a>
                    </li>
                    <li>
                        <a class="dropdown-item" href="<?= base_url('login/logout') ?>">Keluar</a>
                    </li>
              </div>
          </div>
      </div>
      <div class="row">
        <div class="col-sm-3 menu-penduduk mt-4">
            <div class="list-group list-group-flush">
            <a href="<?= base_url('penduduk/dashboard') ?>" class="list-group-item list-group-item-action <?php echo $this->uri->segment(2) == 'dashboard' ? 'active': '' ?>">
                Beranda
            </a>
            <a href="<?= base_url('penduduk/produk') ?>" class="list-group-item list-group-item-action <?php echo $this->uri->segment(2) == 'produk' ? 'active': '' ?>">Produk</a>
            <a href="<?= base_url('penduduk/toko') ?>" class="list-group-item list-group-item-action <?php echo $this->uri->segment(2) == 'toko' ? 'active': '' ?>">Kelola Lapak</a>
            </div>
        </div>