
<?php  if($this->uri->segment('1') != 'p' && $this->uri->segment('1') != 'artikel' && $this->uri->segment('1') != 'produk'){ ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?php $query = $this->db->query("SELECT * FROM tbl_setting WHERE id=1")->result_array(); foreach ($query as $site) :?>
            <link rel="shorcut icon" href="<?php echo base_url().'theme/images/logo/'?><?= $site['logo']; ?>">
            <title><?= $site['site_title']; ?> | <?= $site['description']; ?></title>
            <meta name="description" content="<?= $site['description']; ?>">
        
            <meta name="author" content="<?= $site['site_title']; ?>">
            <meta name="keywords" content="<?= $site['site_title']; ?>, Kedungotok, Tembelang, Jombang, Jawa Timur">
            <meta property="og:url" content="<?php echo base_url()?>">
            <meta property="og:site_name" content="<?= $site['site_title']; ?>">
            <meta property="og:description" content="Website Resmi <?= $site['site_title']; ?>">
            <meta property="og:image" content="<?php echo base_url().'theme/images/logo/'?><?= $site['logo']; ?>">
            <meta property="twitter:site" content="<?= $site['site_title']; ?>">
            <meta property="twitter:site:id" content="">
            <meta property="twitter:card" content="summary">
            <meta property="twitter:description" content="<?= $site['description']; ?>">
            <meta property="twitter:image:src" content="<?php echo base_url().'theme/images/logo/'?><?= $site['logo']; ?>">
        <?php endforeach;?>
<?php } ?>
<?php  if($this->uri->segment('1') == 'artikel' || $this->uri->segment('1') == 'produk'){ ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?php $query = $this->db->query("SELECT * FROM tbl_setting WHERE id=1")->result_array(); foreach ($query as $site) :?>
            <link rel="shorcut icon" href="<?php echo base_url().'theme/images/logo/'?><?= $site['logo']; ?>">
            <title><?= $site['site_title']; ?> | <?= $title ?></title>
        <?php endforeach ?>
<?php } ?>
    
<meta name="google-site-verification" content="9EghVfHGGZ3GTSy8G0OgD3_p6SLXORPVWFTsSY9YMjY" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url().'theme/css/bootstrap.min.css'?>">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url().'theme/css/font-awesome.min.css'?>">
    <!-- Simple Line Font -->
    <link rel="stylesheet" href="<?php echo base_url().'theme/css/simple-line-icons.css'?>">
    <!-- Slider / Carousel -->
    <link rel="stylesheet" href="<?php echo base_url().'theme/css/slick.css'?>">
    <link rel="stylesheet" href="<?php echo base_url().'theme/css/slick-theme.css'?>">
    <link rel="stylesheet" href="<?php echo base_url().'theme/css/owl.carousel.min.css'?>">
    <!-- Main CSS -->
    <link href="<?php echo base_url().'theme/css/style.css'?>" rel="stylesheet">
    <!--Social Share-->
    <link href="<?php echo base_url().'theme/css/jssocials.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'theme/css/jssocials-theme-flat.css'?>" rel="stylesheet">
      <!-- Magnific Popup CSS -->
    <link rel="stylesheet" href="<?php echo base_url().'theme/css/magnific-popup.css'?>">
    <!-- Image Hover CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/normalize.css'?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/set2.css'?>" />
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/style.css'?>" />

    <!-- Masonry Gallery -->
    <link href="<?php echo base_url().'theme/css/animated-masonry-gallery.css'?>" rel="stylesheet" type="text/css" />
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-BN07MEZ0GW"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-BN07MEZ0GW');
    </script>

    <?php
        function limit_words($string, $word_limit){
            $words = explode(" ",$string);
            return implode(" ",array_splice($words,0,$word_limit));
        }
    ?>

    <style>
    	.sharePopup{
    		font-size: 11px;
    	}
      .sharePopup a{
    		font-size: 11px;
        color: #fff;
        text-decoration: none;
    	}
    </style>

</head>

<body>
    <!--============================= HEADER =============================-->
    <div class="header-topbar">
        <div class="container">
            <div class="row">
                <div class="col-xs-8 col-sm-10 col-md-9">
                    <?php $query = $this->db->query("SELECT * FROM tbl_setting WHERE id=1")->result_array(); foreach ($query as $kontak) :?>
                    <div class="header-top_address">
                        <div class="header-top_list">
                            <span class="icon-phone"></span><?= $kontak['no_telephone']; ?>
                        </div>
                        <div class="header-top_list">
                            <span class="icon-envelope-open"></span><?= $kontak['email']; ?>
                        </div>
                        <div class="header-top_list">
                            <span class="icon-location-pin"></span><?= $kontak['address']; ?>
                        </div>
                    </div>
                    <?php endforeach ?> 
                    <div class="header-top_login2">
                        <?php if($this->session->userdata('penduduk_masuk') == TRUE){ ?>
                            <a href="<?php echo site_url('penduduk/dashboard');?>"><span class="material-icons vm">account_circle</span> Akun</a>
                        <?php }elseif($this->session->userdata('masuk') == TRUE){ ?>
                            <a href="<?php echo site_url('admin/dashboard');?>"><span class="material-icons vm">account_circle</span> Akun</a>
                        <?php }else{ ?>
                            <a href="<?php echo site_url('login');?>"> Login</a> | <a href="<?php echo site_url('daftar');?>"> Daftar</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-2 col-md-3">
                    <div class="header-top_login mr-sm-3">
                    <?php if($this->session->userdata('penduduk_masuk') == TRUE){ ?>
                        <a href="<?php echo site_url('penduduk/dashboard');?>"><span class="material-icons vm">account_circle</span> Akun</a>
                    <?php }elseif($this->session->userdata('masuk') == TRUE){ ?>
                        <a href="<?php echo site_url('admin/dashboard');?>"><span class="material-icons vm">account_circle</span> Akun</a>
                    <?php }else{ ?>
                        <a href="<?php echo site_url('login');?>"> Login</a> | <a href="<?php echo site_url('daftar');?>"> Daftar</a>
                    <?php } ?>
                    </div>
                </div>
                    
            </div>
        </div>
    </div>
    <div data-toggle="affix">
        <div class="container nav-menu2">
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar navbar2 navbar-toggleable-md navbar-light bg-faded">
                        <button class="navbar-toggler navbar-toggler2 navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown">
                            <span class="icon-menu"></span>
                        </button>
                        <a href="<?php echo site_url('');?>" class="navbar-brand nav-brand2"><img class="img img-responsive" width="50px;" src="<?php echo base_url().'theme/images/logo/'?><?= $kontak['logo']; ?>"> <b><?= $kontak['site_title']; ?></b></a>
                        <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                            <ul class="navbar-nav">
                               <?php
                                    $menu = $this->db->get('tbl_menu')->result();
                                    foreach ($menu as $menus) { 
                                        $id_menu = $menus->id;
                                        $cek_submenu = $this->db->query("SELECT * FROM tbl_submenu WHERE menu_id='$id_menu'");
		                                if($cek_submenu->num_rows() > 0){
                                            echo '
                                            <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                '.$menus->menu_name.'
                                                </a>
                                                <ul class="dropdown-menu">
                                            ';
                                                //$submenu = $this->db->get('tbl_submenu')->result_array();
                                                $submenu = $this->db->query("SELECT * FROM tbl_submenu WHERE menu_id='$id_menu'")->result_array();
                                                foreach ($submenu as $sub) {
                                                   echo '<li><a class="dropdown-item" href="'.base_url().''.$sub['submenu_link'].'">'.$sub['submenu_name'].'</a></li>';
                                                } 
                                                
                                            echo '</ul>
                                            </li>';
                                        }else{
                                           echo '<li class="nav-item">
                                                    <a class="nav-link" href="'.base_url($menus->menu_link).'">'.$menus->menu_name.'</a>
                                                </li>'; 
                                        }
                                    }
                               ?>
                             </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>