<?php $this->load->view('depan/header'); ?>
<!--//END HEADER -->
<!--============================= BLOG =============================-->
<section class="blog-wrap" style="background: #fafafa !important; padding-top: 30px;">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= base_url('/home') ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Produk</li>
            </ol>
        </nav>
        <div class="row">
            <div class="col-md-8">
                <?php echo $this->session->flashdata('msg');?>
                <?php foreach ($data->result_array() as $row) : ?>
                    <div class="blog-card bg-white mb-5 overflow-hidden d-lg-flex rounded-lg position-relative border-radius-7">
                        <div class="blog-image overflow-hidden d-flex align-items-center">
                            <img src="<?= base_url('assets/images/produk/') ?><?= $row['foto']?>" alt="" class="blog-thumbnail">
                        </div>
                        <div class="p-3 blog-container">
                            <h3 class="font-weight-bold d-flex justify-content-between">
                                <a href="<?= base_url('produk/') ?><?= $row['slug']?>" class="text-dark" title="<?= $row['produk']?>">
                                <?php 
                                    if(strlen($row['produk']) > 25){
                                        echo substr($row['produk'], 0, strpos(wordwrap($row['produk'], 25), "\n"));
                                        echo "...";
                                    }else{
                                        echo $row['produk'];
                                    }
                                ?>
                                </a>
                            </h3>
                            
                            <div class="d-flex align-items-baseline pt-2">
                                <h3 class="mr-2 mb-4 font-weight-bold">Rp. <?= number_format($row['harga'])?></h3>
                                <h5 class="text-striked text-muted mr-3 font-weight-regular">/<?= $row['satuan_harga']?></h5>
                            </div>
                            <div class="mb-2">
                                <small>
                                    <?php
                                        if(strlen($row['deskripsi']) > 70){
                                            echo substr($row['deskripsi'], 0, strpos(wordwrap($row['deskripsi'], 70), "\n"));
                                            echo "...";
                                        }else{
                                            echo $row['deskripsi'];
                                        }
                                    ?></small>
                            </div>
                            
                            <div class="blog-footer d-flex justify-content-between align-items-center border-top">
                                <div class="">
                                    <a href="#"><img src="<?= base_url('assets/images/toko/') ?><?= $row['logo']; ?>" alt="" class="blog-author shadow"></a>
                                    <a href="<?= base_url('toko/') ?><?= $row['slug_toko']?>" class="text-dark"><?= substr($row['nama'], 0, 18); ?>...</a>
                                </div>
                                <small class="text-muted"><?php echo date("d M Y", strtotime($row['created_at']));?></small>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
                <nav>
                    <?php error_reporting(0); echo $page;?>
                </nav>
            </div>
            <div class="col-md-4">
                <form action="<?php echo site_url('blog/search');?>" method="get">
                    <input type="text" name="keyword" placeholder="Search" class="blog-search" required>
                    <button type="submit" class="btn btn-warning btn-blogsearch">SEARCH</button>
                </form>
                <div class="blog-category_block">
                  <h3>Kategori Artikel</h3>
                  <ul>
                    <?php foreach ($category->result() as $row) : ?>
                      <li><a href="<?php echo site_url('blog/kategori/'.str_replace(" ","-",$row->kategori_nama));?>"><?php echo $row->kategori_nama;?><i class="fa fa-caret-right" aria-hidden="true"></i></a></li>
                    <?php endforeach;?>
                  </ul>
                </div>
                <div class="blog-featured_post">
                    <h3>Artikel Populer</h3>
                    <?php foreach ($populer->result() as $row) :?>
                      <div class="blog-featured-img_block ">
                          <img width="35%" src="<?php echo base_url().'assets/images/'.$row->tulisan_gambar;?>" class="img-fluid " alt="blog-featured-img">
                          <h5><a href="<?php echo site_url('artikel/'.$row->tulisan_slug);?>"><?php echo $row->tulisan_judul;?></a></h5>
                          <br>
                          <!-- <p><?php echo limit_words($row->tulisan_isi,5).'...';?></p> -->
                      </div>
                      <hr>
                    <?php endforeach;?>
                </div>

            </div>
        </div>
    </div>
</section>
<!--//END BLOG -->
<!--============================= FOOTER =============================-->
<?php $this->load->view('depan/footer'); ?>