<!DOCTYPE html>
<html lang="en">
<?php $query = $this->db->query("SELECT * FROM tbl_setting WHERE id=1")->result_array(); foreach ($query as $site) :?>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shorcut icon" href="<?php echo base_url().'theme/images/logo/'?><?= $site['logo']; ?>">
    <title><?php echo $produk['produk'];?> - <?= $site['site_title']; ?></title>
    <meta name="description" content="<?php echo $produk['produk']; ?>">
    <meta name="author" content="<?= $site['site_title']; ?>">
    <meta name="keywords" content="<?php echo $produk['produk'];?>, <?= $site['site_title']; ?>, <?php echo $produk['deskripsi'];?>">
    <meta property="og:url" content="<?php echo base_url('produk/'.$produk['slug_toko']);?>">
    <meta property="og:site_name" content="<?php echo $produk['produk'];?>">
    <meta property="og:description" content="<?php echo $produk['deskripsi'];?>">
    <meta property="og:image" content="<?php echo base_url().'assets/images/produk/'.$produk['foto']?>">
    <meta property="twitter:site" content="<?php echo $produk['produk'];?>">
    <meta property="twitter:site:id" content="">
    <meta property="twitter:card" content="summary">
    <meta property="twitter:description" content="<?php echo $produk['produk'];?>">
    <meta property="twitter:image:src" content="<?php echo base_url().'assets/images/produk/'.$produk['foto']?>">
<?php endforeach;?>
<?php $this->load->view('layout/header_penduduk'); ?>
<!--//END HEADER -->

<!--============================= BLOG =============================-->
<section class="blog-wrap" style="background: #fafafa !important; padding-top: 30px;">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= base_url('/home') ?>">Home</a></li>
                <li class="breadcrumb-item"><a href="<?= base_url('/produk') ?>">Lapak Desa</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $produk['produk'];?></li>
            </ol>
        </nav>
        <div class="row">
            <div class="col-md-12">
                <div class="blog-card bg-white mb-4 overflow-hidden d-lg-flex rounded-lg position-relative border-radius-7 mt-5">
                    <div class="blog-image blog-image-produk overflow-hidden d-flex align-items-center">
                        <?php
                            $i = 1;
                            $id = $produk['id'];
                            $cek_foto = $this->db->query("SELECT * FROM tbl_foto_produk WHERE id_produk='$id' ORDER BY id ASC");
                            if($cek_foto->num_rows() > 0){
                        ?>
                            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <?php foreach ($cek_foto->result() as $slide) : $item_class = ($i == 1) ? 'active' : '';?>
                                    <div class="carousel-item <?= $item_class; ?>" data-interval="2000">
                                        <img src="<?= base_url('assets/images/produk/slide_produk/') ?><?= $slide->foto?>" class="d-block w-100" alt="...">
                                    </div>
                                    <?php $i++; endforeach ?>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        <?php }else{ ?> 
                            <img src="<?= base_url('assets/images/produk/') ?><?= $produk['foto']?>" alt="" class="blog-thumbnail">
                        <?php } ?>
                    </div>
                    <div class="p-4 blog-container">
                        <h3 class="font-weight-bold d-flex justify-content-between">
                            <a href="#!" class="text-dark" title="<?= $produk['produk']?>">
                                <?= $produk['produk']?>
                            </a>
                        </h3>
                        
                        <div class="d-flex align-items-baseline mb-2 pt-2">
                            <h2 class="mr-2 mb-4 font-weight-bold">Rp. <?= number_format($produk['harga'])?></h2>
                            <h5 class="text-striked text-muted mr-3 font-weight-regular">/<?= $produk['satuan_harga']?></h5>
                        </div>
                        <div class="mb-2 border-top">
                            <p class="text-muted font-weight-bold pt-2"><i class="fa fa-map-marker"></i> Dikirim dari</p>
                            <small class="text-muted"><?= $produk['alamat']?></small>
                        </div>
                        
                        <div class="mb-2 border-top">
                            <p class="text-muted font-weight-bold pt-2"><i class="fa fa-align-left"></i> Deskripsi</p>
                           <small class="text-muted"> <?= $produk['deskripsi']?></small>
                        </div>
                        
                        <div class="mb-2 border-top">
                            <p class="text-muted font-weight-bold pt-2"><i class="fa fa-phone"></i> Hubungi Penjual</p>
                            <div class="row">
                                <div class="col">
                                    <a href="https://wa.me/<?= $produk['no_wa']?>" target="_blank" class="btn btn-outline-success btn-block  mb-3"><i class="fa fa-whatsapp"></i> Whatsapp</a>
                                </div>
                                <div class="col">
                                    <a href="<?= $produk['facebook']?>" target="_blank" class="btn btn-outline-primary btn-block  mb-3"><i class="fa fa-facebook"></i> Facebook</a>
                                </div>
                                <div class="col">
                                    <a href="<?= $produk['instagram']?>" target="_blank" class="btn btn-outline-danger btn-block  mb-3"><i class="fa fa-instagram"></i> Instagram</a>
                                </div>
                                <div class="col">
                                    <a href="tel:<?= $produk['no_telepon']?>" target="_blank" class="btn btn-info btn-block mb-3"><i class="fa fa-phone"></i> Telephone</a>
                                </div>
                            </div>
                        </div>
                        
                        <div class="mb-2 border-top">
                            <p class="text-muted font-weight-bold pt-2"><i class="fa fa-share-alt"></i> Bagikan</p>
                            <div class="sharePopup"></div>
                        </div>
                        
                        <div class="blog-footer d-flex justify-content-between align-items-center border-top">
                            <div class="">
                                <a href="#"><img src="<?= base_url('assets/images/toko/') ?><?= $produk['logo']; ?>" alt="" class="blog-author shadow"></a>
                                <a href="<?= base_url('toko/') ?><?= $produk['slug_toko']?>" class="text-dark"><?= $produk['nama']; ?></a>
                            </div>
                            <small class="text-muted"><?php echo date("d M Y", strtotime($produk['created_at']));?> | Dilihat <?= $produk['view']; ?>x</small>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="container row">
                    <h4 class="font-weight-bold mt-4">Produk Lainnya</h4>
                    <div class="ml-auto mt-4">
                        <a href="<?= base_url('produk') ?>" class="text-theme text-decoration-none">Lihat semua <i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
                <div id="events" class="event-list owl-carousel owl-loaded owl-drag mt-3">
                    <div class="owl-stage-outer">
                        <div class="owl-stage">
                            <?php foreach ($produk_aktif as $data) : ?>
                                <div class="owl-item" style="width: 768px;">
                                    <div class="product-card mb-4 mr-4 overflow-hidden d-lg-flex flex-column position-relative border-radius-7">
                                        <div class="product-image overflow-hidden">
                                            <img src="<?= base_url('assets/images/produk/') ?><?= $data['foto']?>" alt="" class="product-thumbnail">
                                        </div>
                                        <div class="p-4 product-details">
                                            <h4 class="font-weight-bold d-flex justify-content-between">
                                                <a href="<?= base_url('produk/') ?><?= $data['slug']?>" class="text-dark mb-2 mr-3" title="<?= $data['produk']?>">
                                                <?php 
                                                    if(strlen($data['produk']) > 30 ){
                                                        echo substr($data['produk'], 0, strpos(wordwrap($data['produk'], 30), "\n"));
                                                        echo "...";
                                                    }else{
                                                        echo $data['produk'];
                                                    }
                                                ?>
                                                </a>
                                            </h4>
                                            <div class="d-flex align-items-baseline mb-2">
                                                <h3 class="font-weight-bold mr-2">Rp. <?= number_format($data['harga'])?></h3><h5 class="text-striked text-muted mr-3 font-weight-regular">/<?= $data['satuan_harga']?></h5>
                                            </div>
                                            <div class="mb-2">
                                                <small>
                                                    <?php 
                                                        if(strlen($data['deskripsi']) > 50 ){
                                                            echo substr($data['deskripsi'], 0, strpos(wordwrap($data['deskripsi'], 50), "\n"));
                                                            echo "...";
                                                        }else{
                                                            echo $data['deskripsi'];
                                                        }
                                                    ?>
                                                </small>
                                            </div>
                                            <div class="produk-footer d-flex justify-content-left mb-2 pt-2">
                                                <img src="<?= base_url('assets/images/toko/') ?><?= $data['logo']; ?>"  class="produk-author rounded-circle img-fluid shadow-sm" alt="" style="width: auto !important;">
                                                <a href="<?= base_url('toko/') ?><?= $data['slug_toko']?>" class="text-dark pt-2">
                                                    <?= $data['nama']?>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--//END BLOG -->

<!--============================= FOOTER =============================-->
<?php $this->load->view('layout/footer_penduduk'); ?>