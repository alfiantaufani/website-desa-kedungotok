<?php $this->load->view('layout/header_penduduk'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="col-12">
                    <div class="card border-radius-10 shadow border-0 mt-4 bg-cover">
                        <div class="card-header p-3">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="d-flex align-items-center">
                                    <div class="box-image">
                                        <img src="<?= base_url('assets/images/toko/') ?><?= $toko['logo']; ?>"  class="rounded-circle img-fluid" width="100%" alt="">
                                    </div>
                                    <div class="desc-profile ml-3">
                                        <h4 class="text-white">
                                            <b>
                                            <?= $toko['nama_toko'];?>
                                            </b>
                                        </h4>
                                        <small class="text-white">
                                            <?= $toko['alamat_toko']; ?>
                                        </small>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container row">
                <h4 class="font-weight-bold mt-4">Deskripsi Lapak</h4>
            </div>
            <div class="card border-radius-10 shadow border-0 mt-2">
                <div class="card-body pt-4 pl-2">
                    <div class="container">
                        <div class="mb-2">
                            <p class="text-muted font-weight-bold pt-2"><i class="fa fa-align-left"></i> Deskripsi Lapak</p>
                            <small class="text-muted"> <?= $toko['deskripsi']?></small>
                        </div>
                        <div class="mb-2 border-top">
                            <p class="text-muted font-weight-bold pt-2"><i class="fa fa-map-marker"></i> Alamat Lapak</p>
                            <small class="text-muted"><?= $toko['alamat_toko']?></small>
                        </div>
                        <div class="mb-2 border-top">
                            <p class="text-muted font-weight-bold pt-2"><i class="fa fa-phone"></i> Hubungi Lapak</p>
                            <div class="row">
                                <div class="col">
                                    <a href="https://wa.me/<?= $toko['no_wa']?>" target="_blank" class="btn btn-outline-success btn-block  mb-3"><i class="fa fa-whatsapp"></i> Whatsapp</a>
                                </div>
                                <div class="col">
                                    <a href="<?= $toko['facebook']?>" target="_blank" class="btn btn-outline-primary btn-block  mb-3"><i class="fa fa-facebook"></i> Facebook</a>
                                </div>
                                <div class="col">
                                    <a href="<?= $toko['instagram']?>" target="_blank" class="btn btn-outline-danger btn-block  mb-3"><i class="fa fa-instagram"></i> Instagram</a>
                                </div>
                                <div class="col">
                                    <a href="tel:<?= $toko['no_telepon']?>" target="_blank" class="btn btn-info btn-block mb-3"><i class="fa fa-phone"></i> Telephone</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container row">
                <h4 class="font-weight-bold mt-4">Produk Populer</h4>
            </div>
            <div class="row mt-3">
                <?php foreach ($produk_populer as $data) : ?>
                    <div class="col-md-6">
                        <div class="product-card mb-4 overflow-hidden d-lg-flex flex-column position-relative border-radius-7">
                            <div class="product-image overflow-hidden">
                            <img src="<?= base_url('assets/images/produk/') ?><?= $data['foto']?>" alt="" class="product-thumbnail">
                            </div>
                            <div class="p-4 product-details">
                            <h4 class="font-weight-bold d-flex justify-content-between">
                                <a href="<?= base_url('produk/') ?><?= $data['slug']?>" target="_blank" class="text-dark mb-2 mr-3" title="<?= $data['produk']?>">
                                <?php 
                                if(strlen($data['produk']) > 35){
                                    echo substr($data['produk'], 0, strpos(wordwrap($data['produk'], 35), "\n"));
                                    echo "...";
                                }else{
                                    echo $data['produk'];
                                }
                                ?>
                                </a>
                            </h4>
                            <div class="d-flex align-items-baseline">
                                <h3 class="font-weight-bold mr-2 mt-2">Rp. <?= number_format($data['harga'])?></h3><h5 class="text-striked text-muted mr-3 font-weight-regular">/<?= $data['satuan_harga']?></h5>
                            </div>
                            <small>
                                <?php 
                                if(strlen($data['deskripsi']) > 50 ){
                                    echo substr($data['deskripsi'], 0, strpos(wordwrap($data['deskripsi'], 50), "\n"));
                                    echo "...";
                                }else{
                                    echo $data['deskripsi'];
                                }
                                ?>
                            </small>
                            <div class="produk-footer d-flex justify-content-between mb-2 mt-3 pt-2">
                                <div>
                                <img src="<?= base_url('assets/images/toko/') ?><?= $data['logo']; ?>"  class="produk-author rounded-circle img-fluid shadow-sm" alt="">
                                <a href="<?= base_url('toko/') ?><?= $data['slug_toko']?>" class="text-dark">
                                    <?php 
                                    if(strlen($data['nama']) > 20){
                                        echo substr($data['nama'], 0, strpos(wordwrap($data['nama'], 20), "\n"));
                                        echo "...";
                                    }else{
                                        echo $data['nama'];
                                    }
                                    ?>
                                </a>
                                </div>
                                <small class="text-muted pt-2"><?php echo date("d M Y", strtotime($data['created_at']));?></small>
                            </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
            <div class="container row">
                <h4 class="font-weight-bold mt-4">Semua Produk</h4>
            </div>
            <div class="row mt-3">
                <?php foreach ($produk_aktif as $data) : ?>
                    <div class="col-md-6">
                        <div class="product-card mb-4 overflow-hidden d-lg-flex flex-column position-relative border-radius-7">
                            <div class="product-image overflow-hidden">
                            <img src="<?= base_url('assets/images/produk/') ?><?= $data['foto']?>" alt="" class="product-thumbnail">
                            </div>
                            <div class="p-4 product-details">
                            <h4 class="font-weight-bold d-flex justify-content-between">
                                <a href="<?= base_url('produk/') ?><?= $data['slug']?>" target="_blank" class="text-dark mb-2 mr-3" title="<?= $data['produk']?>">
                                <?php 
                                if(strlen($data['produk']) > 35){
                                    echo substr($data['produk'], 0, strpos(wordwrap($data['produk'], 35), "\n"));
                                    echo "...";
                                }else{
                                    echo $data['produk'];
                                }
                                ?>
                                </a>
                            </h4>
                            <div class="d-flex align-items-baseline">
                                <h3 class="font-weight-bold mr-2 mt-2">Rp. <?= number_format($data['harga'])?></h3><h5 class="text-striked text-muted mr-3 font-weight-regular">/<?= $data['satuan_harga']?></h5>
                            </div>
                            <small>
                                <?php 
                                if(strlen($data['deskripsi']) > 50 ){
                                    echo substr($data['deskripsi'], 0, strpos(wordwrap($data['deskripsi'], 50), "\n"));
                                    echo "...";
                                }else{
                                    echo $data['deskripsi'];
                                }
                                ?>
                            </small>
                            <div class="produk-footer d-flex justify-content-between mb-2 mt-3 pt-2">
                                <div>
                                <img src="<?= base_url('assets/images/toko/') ?><?= $data['logo']; ?>"  class="produk-author rounded-circle img-fluid shadow-sm" alt="">
                                <a href="<?= base_url('toko/') ?><?= $data['slug_toko']?>" class="text-dark">
                                    <?php 
                                    if(strlen($data['nama']) > 20){
                                        echo substr($data['nama'], 0, strpos(wordwrap($data['nama'], 20), "\n"));
                                        echo "...";
                                    }else{
                                        echo $data['nama'];
                                    }
                                    ?>
                                </a>
                                </div>
                                <small class="text-muted pt-2"><?php echo date("d M Y", strtotime($data['created_at']));?></small>
                            </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
        <div class="col-md-4 mt-4">
            <form action="<?php echo site_url('blog/search');?>" method="get">
                <input type="text" name="keyword" placeholder="Search" class="blog-search" required>
                <button type="submit" class="btn btn-warning btn-blogsearch">SEARCH</button>
            </form>
            <div class="blog-category_block">
                <h3>Kategori Artikel</h3>
                <ul>
                    <?php foreach ($category->result() as $data) : ?>
                    <li><a href="<?php echo site_url('blog/kategori/'.str_replace(" ","-",$data->kategori_nama));?>"><?php echo $data->kategori_nama;?><i class="fa fa-caret-right" aria-hidden="true"></i></a></li>
                    <?php endforeach;?>
                </ul>
            </div>
            <div class="blog-featured_post">
                <h3>Artikel Populer</h3>
                <?php foreach ($populer->result() as $data) :?>
                    <div class="blog-featured-img_block ">
                        <img width="35%" src="<?php echo base_url().'assets/images/'.$data->tulisan_gambar;?>" class="img-fluid " alt="blog-featured-img">
                        <h5><a href="<?php echo site_url('artikel/'.$data->tulisan_slug);?>"><?php echo $data->tulisan_judul;?></a></h5>
                        <br>
                        <!-- <p><?php echo limit_words($data->tulisan_isi,5).'...';?></p> -->
                    </div>
                    <hr>
                <?php endforeach;?>
            </div>

        </div>
    </div>
</div>
<!--============================= FOOTER =============================-->
<?php $this->load->view('layout/footer_penduduk'); ?>