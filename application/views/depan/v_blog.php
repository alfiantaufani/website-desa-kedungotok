<?php $this->load->view('depan/header'); ?>
<!--//END HEADER -->
<!--============================= BLOG =============================-->
<section class="blog-wrap" style="background: #fafafa !important;">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
              <?php echo $this->session->flashdata('msg');?>
              <?php foreach ($data->result_array() as $row) : ?>
                <div class="col-md-12 mb-4">
                    <!-- <div class="card border-radius-10 shadow border-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-5">
                                    <img src="<?php echo base_url().'assets/images/'.$row->tulisan_gambar;?>" width="100%" class="border-radius-7" alt="">
                                </div>
                                <div class="col-sm-7" style="padding-left: 30px">
                                    <a href="<?php echo site_url('artikel/'.$row->tulisan_slug);?>" class="text-decoration-none text-theme">
                                        <h4 class="font-weight-bold mt-4"><?= substr(($row->tulisan_judul), 0, 33); ?>...</h4>
                                    </a>
                                    <h6 class="mt-3"> 
                                        <a href="#" class="text-muted">
                                            <i class="fa fa-user" aria-hidden="true"></i> <span> <?php echo $row->tulisan_author;?></span> 
                                        </a>  |   
                                        <a href="#" class="text-muted">
                                            <i class="fa fa-tags" aria-hidden="true"></i> <span><?php echo $row->tulisan_kategori_nama;?></span>
                                        </a> |
                                        <a href="#" class="text-muted">
                                            <i class="fa fa-calendar" aria-hidden="true"></i> <span><?php echo substr(($row->tulisan_tanggal), 0, 10);?></span>
                                        </a> 
                                    </h6>
                                    <p class="text-muted"><?php echo substr(($row->tulisan_deskripsi), 0, 120);?>...</p>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
                <div class="blog-card bg-white mb-3 overflow-hidden d-lg-flex rounded-lg position-relative border-radius-7">
                    <div class="blog-image overflow-hidden d-flex align-items-center">
                        <img src="<?= base_url('assets/images/') ?><?= $row['tulisan_gambar']?>" alt="" class="blog-thumbnail">
                    </div>
                    <div class="p-3 blog-container">
                        <a href="<?php echo site_url('blog/kategori/'.str_replace(" ","-",$row['tulisan_kategori_nama']));?>" class="blog-category text-uppercase py-1 px-2 rounded-lg">
							<small class="font-weight-bold"><?= $row['tulisan_kategori_nama'] ?></small>
						</a>
                        <h3 class="font-weight-bold d-flex justify-content-between mt-3">
                            <a href="<?= base_url('artikel/') ?><?= $row['tulisan_slug']?>" class="text-dark" title="<?= $row['tulisan_judul']?>" style="line-height: 1.3;">
                            <?php 
                                if(strlen($row['tulisan_judul']) > 52){
                                    echo substr($row['tulisan_judul'], 0, strpos(wordwrap($row['tulisan_judul'], 52), "\n"));
                                    echo "...";
                                }else{
                                    echo $row['tulisan_judul'];
                                }
                            ?>
                            </a>
                        </h3>
                        
                        <div class="mb-1">
                            <small>
                            <?php 
                                if(strlen($row['tulisan_deskripsi']) > 200){
                                    echo substr($row['tulisan_deskripsi'], 0, strpos(wordwrap($row['tulisan_deskripsi'], 200), "\n"));
                                    echo "...";
                                }else{
                                    echo $row['tulisan_deskripsi'];
                                }
                            ?>
                            </small>
                        </div>
                        
                        <div class="blog-footer d-flex justify-content-between align-items-center border-top">
                            <small class="">
                                <a href="#" class="text-dark">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                    <?= $row['tulisan_author']; ?>
                                </a>
                            </small>
                            <small class="text-muted"><?php echo date("d M Y", strtotime($row['tulisan_tanggal']));?></small>
                        </div>
                    </div>
                </div>
              <?php endforeach ?>
                <nav>
                    <?php error_reporting(0); echo $page;?>
                </nav>
            </div>
            <div class="col-md-4">
                <form action="<?php echo site_url('blog/search');?>" method="get">
                    <input type="text" name="keyword" placeholder="Search" class="blog-search" required>
                    <button type="submit" class="btn btn-warning btn-blogsearch">SEARCH</button>
                </form>
                <div class="blog-category_block">
                  <h3>Kategori Artikel</h3>
                  <ul>
                    <?php foreach ($category->result() as $row) : ?>
                      <li><a href="<?php echo site_url('blog/kategori/'.str_replace(" ","-",$row->kategori_nama));?>"><?php echo $row->kategori_nama;?><i class="fa fa-caret-right" aria-hidden="true"></i></a></li>
                    <?php endforeach;?>
                  </ul>
                </div>
                <div class="blog-featured_post">
                    <h3>Artikel Populer</h3>
                    <?php foreach ($populer->result() as $row) :?>
                      <div class="blog-featured-img_block ">
                          <img width="35%" src="<?php echo base_url().'assets/images/'.$row->tulisan_gambar;?>" class="img-fluid " alt="blog-featured-img">
                          <h5><a href="<?php echo site_url('artikel/'.$row->tulisan_slug);?>"><?php echo $row->tulisan_judul;?></a></h5>
                          <br>
                          <!-- <p><?php echo limit_words($row->tulisan_isi,5).'...';?></p> -->
                      </div>
                      <hr>
                    <?php endforeach;?>
                </div>

            </div>
        </div>
    </div>
</section>
<!--//END BLOG -->
<!--============================= FOOTER =============================-->
<?php $this->load->view('depan/footer'); ?>