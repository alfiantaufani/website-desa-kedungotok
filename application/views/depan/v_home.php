<?php date_default_timezone_set('Asia/Jakarta'); $this->load->view('depan/header'); ?>

<section>
    <div class="slider_img layout_two">
        <div id="carousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <?php 
                    $cek = $this->db->query("SELECT COUNT(*) AS bnyk FROM tbl_slider")->result_array();
                    foreach ($cek as $d) {
                        $no = $d['bnyk'];
                        for ($no=0; $no < 0; $no++) { 
                ?>
                    <li data-target="#carousel" data-slide-to="<?=$no ?>" class="<?php if($no == 0){echo'active';}else{echo ' ';}?>"></li>

                <?php   }
                    }
                ?>
                
            </ol>
            <div class="carousel-inner" role="listbox">
                <?php 
                    $no= 0;
                    $slider = $this->db->query("SELECT * FROM tbl_slider ORDER BY id ASC")->result_array();
                    foreach ($slider as $slide) :
                ?>
                <div class="carousel-item <?php if($no == 0){echo 'active';} ?>">
                    <img class="d-block" src="<?php echo base_url().'assets/images/slider/'.$slide['image'];?>" alt="First slide">
                    <div class="carousel-caption d-md-block">
                        <div class="slider_title">
                            <h1><?= $slide['caption'] ?></h1>
                            <h4><?= $slide['description'] ?></h4>
                            <div class="slider-btn">
                                <a href="<?= $slide['url'] ?>" class="btn btn-default">Selengkapnya</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $no++; endforeach ?>
            </div>
            <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                <i class="icon-arrow-left fa-slider" aria-hidden="true"></i>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                <i class="icon-arrow-right fa-slider" aria-hidden="true"></i>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</section>
<!--//END HEADER -->
<!--============================= ABOUT =============================-->
<section class="clearfix about about-style2">
    <div class="container">
        <div class="row">
            <?php $query = $this->db->query("SELECT * FROM tbl_setting WHERE id=1")->result_array(); foreach ($query as $data) :?>
            <div class="col-md-9">
               <h2>Sambutan Kepala Desa</h2>
               <p><?= $data['text_home']; ?></p> 
            </div>
            <div class="col-md-3">
                <img src="<?php echo base_url().'theme/images/kepsek/'.$data['foto_kepsek'].''?>" class="img-fluid about-img" alt="foto Kepala Desa">
                <p class="text-center"><b><?= $data['nama_kepsek']; ?></b></p>
            </div>
            <?php endforeach ?>
        </div>
    </div>
</section>
<!--//END ABOUT -->
<!--============================= OUR COURSES =============================-->
<section class="our_courses">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Artikel Populer</h2>
            </div>
        </div>
        <div class="row">
          <?php foreach ($berita->result() as $row) :?>
            <div class="col-md-4 mb-4">
                <div class="card border-radius-10 shadow border-0">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <img src="<?php echo base_url().'assets/images/'.$row->tulisan_gambar;?>" width="100%" class="border-radius-7" alt="">
                            </div>
                            <div class="col-sm-12">
                                <div class="container">
                                <a href="<?php echo site_url('artikel/'.$row->tulisan_slug);?>" class="text-decoration-none text-theme">
                                    <h4 class="font-weight-bold mt-4">
                                        <?php
                                        if(strlen($row->tulisan_judul) > 30){
                                            echo substr($row->tulisan_judul, 0, strpos(wordwrap($row->tulisan_judul, 30), "\n"));
                                            echo "...";
                                        }else{
                                            echo $row->tulisan_judul;
                                        }
                                        ?>
                                    </h4>
                                </a>
                                <h6 class="mt-3"> 
                                    <a href="#" class="text-muted">
                                        <i class="fa fa-user" aria-hidden="true"></i> <small> <?php echo $row->tulisan_author;?></small> 
                                    </a>  |   
                                    <a href="#" class="text-muted">
                                        <i class="fa fa-tags" aria-hidden="true"></i> <small><?php echo $row->tulisan_kategori_nama;?></small>
                                    </a> |
                                    <a href="#" class="text-muted">
                                        <i class="fa fa-calendar" aria-hidden="true"></i> <small><?php echo substr(($row->tulisan_tanggal), 0, 10);?></small>
                                    </a> 
                                </h6>
                                <p class="text-muted">
                                    <?php
                                        if(strlen($row->tulisan_deskripsi) > 140){
                                            echo substr($row->tulisan_deskripsi, 0, strpos(wordwrap($row->tulisan_deskripsi, 140), "\n"));
                                            echo "...";
                                        }else{
                                            echo $row->tulisan_deskripsi;
                                        }
                                    ?>
                                </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <div class="courses_box mb-4">
                    <div class="course-img-wrap">
                        <img src="<?php echo base_url().'assets/images/'.$row->tulisan_gambar;?>" class="img-fluid" alt="courses-img">
                    </div>
                    <a href="<?php echo site_url('artikel/'.$row->tulisan_slug);?>" class="course-box-content">
                        <h4 style="text-align:left;"><?php echo $row->tulisan_judul;?></h4>
                        <small><?php echo $row->tulisan_author;?> | <?php echo $row->tulisan_tanggal;?></small>
                    </a>
                </div>
            </div> -->
          <?php endforeach;?>
        </div> <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <a href="<?php echo site_url('artikel');?>" class="btn btn-default btn-courses">Lihat Semua</a>
            </div>
        </div>
    </div>
</section>
<!--//END OUR COURSES -->
<!--============================= EVENTS =============================-->
<section class="event">
    <div class="container">
        <div class="row">
            <!-- <div class="col-lg-6">
                <h2>Pengumuman Terbaru</h2>
                <div class="event-img2">
                <?php foreach ($pengumuman->result() as $row) :?>
                    <div class="row">
                        <div class="blog-card bg-white mb-5 overflow-hidden d-lg-flex rounded-lg position-relative border-radius-7 p-3">
                            <div class="blog-image overflow-hidden d-flex align-items-center">
                                <img src="<?php echo base_url().'theme/images/announcement-icon.png'?>" class="img-fluid" alt="event-img">
                            </div>
                            <div class="p-3 blog-container">
                                <h3><a href="<?php echo site_url('pengumuman');?>">
                                    <?php 
                                        if(strlen($row->pengumuman_judul) > 25){
                                            echo substr($row->pengumuman_judul, 0, strpos(wordwrap($row->pengumuman_judul, 25), "\n"));
                                            echo "...";
                                        }else{
                                            echo $row->pengumuman_judul;
                                        }
                                    ?>
                                    </a>
                                </h3>
                                <span><?php echo $row->tanggal;?></span>
                                <p><?php 
                                    if(strlen($row->pengumuman_deskripsi) > 30){
                                        echo substr($row->pengumuman_deskripsi, 0, strpos(wordwrap($row->pengumuman_deskripsi, 30), "\n"));
                                        echo "...";
                                    }else{
                                        echo $row->pengumuman_deskripsi;
                                    }
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
                </div>
            </div> -->
            <div class="col-lg-6">
                <h2>Pengumuman Terbaru</h2>
                <div class="row">
                    <div class="col-md-12">
                      <?php foreach ($pengumuman->result() as $row):?>
                        <div class="event_date" style="background-color: #fff;">
                            <img src="<?php echo base_url().'theme/images/announcement-icon.png'?>" class="img-fluid" alt="event-img">
                        </div>
                        <div class="date-description">
                            <h3><a href="<?php echo site_url('pengumuman');?>">
                                <?php
                                if(strlen($row->pengumuman_judul) > 25){
                                    echo substr($row->pengumuman_judul, 0, strpos(wordwrap($row->pengumuman_judul, 25), "\n"));
                                    echo "...";
                                }else{
                                    echo $row->pengumuman_judul;
                                }?>
                                </a>
                            </h3>
                            <span><?php echo $row->tanggal;?></span>
                            <p>
                            <?php
                                if(strlen($row->pengumuman_deskripsi) > 30){
                                    echo substr($row->pengumuman_deskripsi, 0, strpos(wordwrap($row->pengumuman_deskripsi, 30), "\n"));
                                    echo "...";
                                }else{
                                    echo $row->pengumuman_deskripsi;
                                }?>
                                
                            </p>
                            <hr class="event_line">
                        </div>
                        <?php endforeach;?>

                    </div>
                </div>

            </div>
            <div class="col-lg-6">
                <h2>Agenda Terbaru</h2>
                <div class="row">
                    <div class="col-md-12">
                      <?php foreach ($agenda->result() as $row):?>
                        <div class="event_date">
                            <div class="event-date-wrap">
                                <p><?php echo date("d", strtotime($row->agenda_tanggal));?></p>
                                <span><?php echo date("M. y", strtotime($row->agenda_tanggal));?></span>
                            </div>
                        </div>
                        <div class="date-description">
                            <h3><a href="<?php echo site_url('agenda');?>">
                                <?php
                                if(strlen($row->agenda_nama) > 25){
                                    echo substr($row->agenda_nama, 0, strpos(wordwrap($row->agenda_nama, 25), "\n"));
                                    echo "...";
                                }else{
                                    echo $row->agenda_nama;
                                }?>
                                </a>
                            </h3>
                            <p>
                            <?php
                                if(strlen($row->agenda_deskripsi) > 30){
                                    echo substr($row->agenda_deskripsi, 0, strpos(wordwrap($row->agenda_deskripsi, 30), "\n"));
                                    echo "...";
                                }else{
                                    echo $row->agenda_deskripsi;
                                }?>
                                
                            </p>
                            <hr class="event_line">
                        </div>
                        <?php endforeach;?>

                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<!--//END EVENTS -->

<section class="our_courses">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Peta Lokasi</h2>
            </div>
        </div>
        <div class="col-md-12">
        <?php $query = $this->db->query("SELECT * FROM tbl_setting WHERE id=1")->result_array(); foreach ($query as $map) :?>
            <?= $map['maps']; ?>
        <?php endforeach;?>
        </div>
    </div>
</section>
<!--============================= FOOTER =============================-->
<?php $this->load->view('depan/footer'); ?>