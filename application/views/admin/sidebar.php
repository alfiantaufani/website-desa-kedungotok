<?php 
  $query2=$this->db->query("SELECT * FROM tbl_komentar WHERE komentar_status='0'");
  $jum_comment=$query2->num_rows();

  $produk_new = $this->db->query("SELECT * FROM tbl_produk WHERE status='Pending'");
  $jum_produk = $produk_new->num_rows();

  $query = $this->db->query("SELECT * FROM tbl_inbox WHERE inbox_status='1'");
  $jum_pesan = $query->num_rows();
?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">Menu Utama</li>
        <li class="<?php echo $this->uri->segment(2) == 'dashboard' ? 'active': '' ?>">
          <a href="<?php echo base_url().'admin/dashboard'?>">
            <i class="fa fa-home"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        <li class="header">Informasi  Desa</li>
        <li class="treeview <?php echo $this->uri->segment(2) == 'tulisan' ? 'active': '' ?>">
          <a href="#">
            <i class="fa fa-newspaper"></i>
            <span>Artikel</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url().'admin/tulisan'?>"><i class="fa fa-list"></i> List Artikel</a></li>
            <li><a href="<?php echo base_url().'admin/tulisan/add_tulisan'?>"><i class="fa fa-thumbtack"></i> Post Artikel</a></li>
            <li><a href="<?php echo base_url().'admin/kategori'?>"><i class="fa fa-wrench"></i> Kategori</a></li>
          </ul>
        </li>
        <?php if($this->session->userdata('akses')=='1'):?>
        <li class="<?php echo $this->uri->segment(2) == 'agenda' ? 'active': '' ?>">
          <a href="<?php echo base_url().'admin/agenda'?>">
            <i class="fa fa-calendar"></i> <span>Agenda</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        <li class="<?php echo $this->uri->segment(2) == 'pengumuman' ? 'active': '' ?>">
          <a href="<?php echo base_url().'admin/pengumuman'?>">
            <i class="fa fa-bullhorn"></i> <span>Pengumuman</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        <li class="<?php echo $this->uri->segment(2) == 'files' ? 'active': '' ?>">
          <a href="<?php echo base_url().'admin/files'?>">
            <i class="fa fa-download"></i> <span>Download</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        <?php endif ?>
        <li class="header">Galeri  Desa</li>
        <li class="treeview <?php echo $this->uri->segment(2) == 'galeri' ? 'active': '' ?>">
          <a href="#">
            <i class="fa fa-camera"></i>
            <span>Galeri Foto</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url().'admin/galeri'?>"><i class="fa fa-images"></i> Photos</a></li>
          </ul>
        </li>
        <li class="header">Pemdes & Penduduk</li>
        <?php if($this->session->userdata('akses')=='1'):?>
        <li class="<?php echo $this->uri->segment(2) == 'staff' ? 'active': '' ?>">
          <a href="<?php echo base_url().'admin/staff'?>">
            <i class="fa fa-university"></i> <span>Pemerintah Desa</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        <?php endif ?>
        <li class="<?php echo $this->uri->segment(2) == 'penduduk' ? 'active': '' ?>">
          <a href="<?php echo base_url().'admin/penduduk/penduduk'?>">
            <i class="fa fa-user-friends"></i> <span>Data Penduduk</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        <li class="<?php echo $this->uri->segment(2) == 'toko' ? 'active': '' ?>">
          <a href="<?php echo base_url().'admin/toko/toko'?>">
            <i class="fa fa-store-alt"></i> <span>Lapak Penduduk</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        <li class="<?php echo $this->uri->segment(2) == 'produk' ? 'active': '' ?>">
          <a href="<?php echo base_url().'admin/produk/produk'?>">
            <i class="fa fa-shopping-basket"></i> <span>Produk Penduduk</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green"><?php echo $jum_produk;?></small>
            </span>
          </a>
        </li>
        <?php if($this->session->userdata('akses')=='1'):?>
        <li class="header">Setelan Website</li>
        <li class="treeview <?php echo $this->uri->segment(2) == 'content' ? 'active': '' ?>">
          <a href="#">
            <i class="fa fa-list-ul"></i>
            <span>Main Konten</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url().'admin/content/pages'?>"><i class="fa fa-file"></i> Halaman</a></li>
            <li><a href="<?php echo base_url().'admin/content/menus'?>"><i class="fa fa-th-list"></i> Menu Utama</a></li>
            <li><a href="<?php echo base_url().'admin/content/submenus'?>"><i class="fa fa-list-ol"></i> Sub Menu</a></li>
          </ul>
        </li>
        <li class="<?php echo $this->uri->segment(2) == 'sliders' ? 'active': '' ?>">
          <a href="<?php echo base_url().'admin/sliders/slider'?>">
            <i class="fa fa-columns"></i> <span>Slider</span>
          </a>
        </li>
        <li class="<?php echo $this->uri->segment(2) == 'inbox' ? 'active': '' ?>">
          <a href="<?php echo base_url().'admin/inbox'?>">
            <i class="fa fa-envelope"></i> <span>Pesan Masuk</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green"><?php echo $jum_pesan;?></small>
            </span>
          </a>
        </li>
        <?php endif ?>
        <li class="<?php echo $this->uri->segment(2) == 'komentar' ? 'active': '' ?>">
          <a href="<?php echo base_url().'admin/komentar'?>">
            <i class="fa fa-comments"></i> <span>Komentar</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green"><?php echo $jum_comment;?></small>
            </span>
          </a>
        </li>
        <li class="<?php echo $this->uri->segment(2) == 'pengguna' ? 'active': '' ?>">
          <a href="<?php echo base_url().'admin/pengguna'?>">
            <i class="fa fa-users"></i> <span>Pengguna</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>
        <?php if($this->session->userdata('akses')=='1'):?>
        <li class="<?php echo $this->uri->segment(2) == 'settings' ? 'active': '' ?>">
          <a href="<?php echo base_url().'admin/settings/settings'?>">
            <i class="fa fa-cog"></i> <span>Pengaturan</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a> 
        </li>
        <?php endif ?>
        <li>
          <a href="<?php echo base_url('/login/logout')?>">
            <i class="fa fa-lock"></i> <span>Keluar</span>
            <span class="pull-right-container">
              <small class="label pull-right"></small>
            </span>
          </a>
        </li>


      </ul>
    </section>
    <!-- /.sidebar -->
</aside>