<!--Counter Inbox-->
<?php
error_reporting(0);
    $query=$this->db->query("SELECT * FROM tbl_inbox WHERE inbox_status='1'");
    $query2=$this->db->query("SELECT * FROM tbl_komentar WHERE komentar_status='0'");
    $jum_comment=$query2->num_rows();
    $jum_pesan=$query->num_rows();
?>
<?php $query = $this->db->query("SELECT * FROM tbl_setting WHERE id=1")->result_array(); foreach ($query as $site) :?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= $site['site_title']; ?> | <?= $title ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="shorcut icon" href="<?php echo base_url().'theme/images/logo/'?><?= $site['logo']; ?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/bootstrap/css/bootstrap.min.css'?>">
  <!-- Font Awesome -->
  <link href="https://desa-v2.sidedi.id/assets/css/font-awesome.min.css" rel="stylesheet" />
  <link href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" rel="stylesheet" />
  <!-- <link rel="stylesheet" href="<?php echo base_url().'assets/font-awesome/css/font-awesome.min.css'?>"> -->
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/plugins/datatables/dataTables.bootstrap.css'?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/dist/css/AdminLTE.css'?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/dist/css/skins/_all-skins.min.css'?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.css'?>"/>



</head>
<?php endforeach ?>
<body class="hold-transition skin-blue sidebar-mini fixed">
<div class="wrapper">

   <?php
    $this->load->view('admin/v_header');
    $this->load->view('admin/sidebar');
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Produk Penduduk
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Produk Penduduk</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">

          <div class="box">
            <div class="box-header">
              <!-- <a class="btn btn-success btn-flat" data-toggle="modal" data-target="#myModal"><span class="fa fa-plus"></span> Add Data Penduduk</a> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-striped" style="font-size:13px;">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Produk</th>
                    <th>Harga</th>
                    <th>Pemilik Lapak</th>
                    <th>Status</th>
                    <th style="text-align:right;">Aksi</th>
                </tr>
                </thead>
                <tbody>
                    <?php
                        $no=1;
                        foreach ($produk->result_array() as $data) :
                    ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $data['produk'] ?></td>
                    <td>Rp. <?= number_format($data['harga']) ?>/<?= $data['satuan_harga'] ?></td>
                    <td><?= $data['nama'] ?></td>
                    <td>
                        <?php if($data['status'] == 'Aktif') : ?>
                            <h5><span class="label label-primary"><?= $data['status'] ?></span></h5>
                        <?php else: ?>
                            <h5><span class="label label-danger"><?= $data['status'] ?></span></h5>
                        <?php endif ?>
                    </td>
                    <td style="text-align:right;">
                        <a class="btn" data-toggle="modal" data-target="#ModalEdit<?= $data['id_produk'] ?>"><span class="fa fa-pencil"></span></a>
                    </td>
                </tr>
				<?php endforeach;?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $this->load->view('admin/footer');?>
<!-- ./wrapper -->

    <!--Modal Edit Album-->
    <?php foreach ($produk->result_array() as $data) :?>
        <div class="modal fade" id="ModalEdit<?= $data['id_produk'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="fa fa-close"></span></span></button>
                        <h4 class="modal-title" id="myModalLabel">Detail Data Produk Penduduk</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'admin/produk/produk/update'?>/<?= $data['id_produk'];?>" method="post" enctype="multipart/form-data">
                        <div class="modal-body">
                            <div class="box-body">
                                <?php
                                    $i = 1;
                                    $id = $data['id_produk'];
                                    $cek_foto = $this->db->query("SELECT * FROM tbl_foto_produk WHERE id_produk='$id' ORDER BY id ASC");
                                    if($cek_foto->num_rows() > 0){
                                ?>
                                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner" role="listbox">
                                        <?php foreach ($cek_foto->result() as $slide) : $item_class = ($i == 1) ? 'active' : '';?>
                                        <div class="item <?= $item_class; ?>" data-interval="3000">
                                            <img src="<?= base_url('assets/images/produk/slide_produk/') ?><?= $slide->foto?>" class="d-block w-100" alt="...">
                                        </div>
                                        <?php $i++; endforeach ?>
                                        
                                    </div>

                                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <?php }else{ ?> 
                                    <img class="img-responsive pad" src="<?= base_url('assets/images/produk/') ?><?= $data['foto']?>" alt="Photo">
                                <?php } ?>
                                
                                <div class="box-body">
                                    <h3 class="font-weight-bold d-flex justify-content-between">
                                        <b><?= $data['produk']?></b>
                                    </h3>
                                    <div class="d-flex align-items-baseline pt-2">
                                        <h2 class="mr-2 mb-4 font-weight-bold">
                                            Rp. <?= number_format($data['harga'])?><small class="text-striked text-muted mr-3 font-weight-regular">/<?= $data['satuan_harga']?></small>
                                        </h2>
                                    </div>
                                    <hr>
                                    <strong><i class="fa fa-align-left"></i> Deskripsi</strong>
                                    <p class="text-muted">
                                        <?= $data['deskripsi']?>
                                    </p>
                                    <hr>
                                    <strong><i class="fa fa-map-marker-alt margin-r-5"></i> Dikirim dari</strong>
                                    <p class="text-muted"><?= $data['alamat']?></p>
                                    <hr>
                                    <strong><i class="fa fa-phone margin-r-5"></i> Hubungi Penjual <br></strong>
                                    
                                    <p style="margin-top: 5px">
                                        <a href="https://wa.me/<?= $data['no_wa']?>" target="_blank" class="btn btn-success mb-3"><i class="fab fa-whatsapp"></i> Whatsapp</a>
                                        <a href="<?= $data['facebook']?>" target="_blank" class="btn btn-primary mb-3"><i class="fab fa-facebook"></i> Facebook</a>
                                        <a href="<?= $data['instagram']?>" target="_blank" class="btn btn-danger mb-3"><i class="fab fa-instagram"></i> Instagram</a>
                                        <a href="tel:<?= $data['no_telepon']?>" target="_blank" class="btn btn-info mb-3"><i class="fa fa-phone"></i> Telephone</a>
                                    </p>
                                    <hr>
                                    <div class="user-block">
                                        <img class="img-circle img-bordered-sm" src="<?= base_url('assets/images/toko/') ?><?= $data['logo']; ?>" alt="user image">
                                            <span class="username">
                                                <a href="<?= base_url('toko/') ?><?= $data['slug_toko']?>"><?= $data['nama'] ?></a>
                                            </span>
                                        <span class="description">Diupload tanggal <?php echo date("d M Y", strtotime($data['created_at']));?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label for="inputUserName" class="col-sm-4 control-label">Status Produk</label>
                            <div class="col-sm-7">
                                <?php if($data['status'] == 'Aktif'):?>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" id="inlineRadio1" value="Aktif" name="status" checked>
                                    <label for="inlineRadio1"> Aktif </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" id="inlineRadio1" value="Pending" name="status">
                                    <label for="inlineRadio2"> Pending </label>
                                </div>
                                <?php else:?>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" id="inlineRadio1" value="Aktif" name="status">
                                    <label for="inlineRadio1"> Aktif </label>
                                </div>
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" id="inlineRadio1" value="Pending" name="status" checked>
                                    <label for="inlineRadio2"> Pending </label>
                                </div>
                                <?php endif;?>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <?php endforeach;?>
	<!--Modal Edit Album-->



<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url().'assets/plugins/jQuery/jquery-2.2.3.min.js'?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url().'assets/bootstrap/js/bootstrap.min.js'?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url().'assets/plugins/datatables/jquery.dataTables.min.js'?>"></script>
<script src="<?php echo base_url().'assets/plugins/datatables/dataTables.bootstrap.min.js'?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url().'assets/plugins/slimScroll/jquery.slimscroll.min.js'?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url().'assets/plugins/fastclick/fastclick.js'?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url().'assets/dist/js/app.min.js'?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url().'assets/dist/js/demo.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.js'?>"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
    <?php if($this->session->flashdata('msg')=='error'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Error',
                    text: "Data Produk gagal diubah",
                    showHideTransition: 'slide',
                    icon: 'error',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#FF4859'
                });
        </script>

    <?php elseif($this->session->flashdata('msg')=='error-password'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Error',
                    text: "Password dan Ulangi Password yang Anda masukan tidak sama.",
                    showHideTransition: 'slide',
                    icon: 'error',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#FF4859'
                });
        </script>

    <?php elseif($this->session->flashdata('msg')=='success'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Produk Berhasil disimpan.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
        </script>

    <?php elseif($this->session->flashdata('msg')=='info'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Info',
                    text: "Produk berhasil di update",
                    showHideTransition: 'slide',
                    icon: 'info',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#00C9E6'
                });
        </script>
    <?php elseif($this->session->flashdata('msg')=='success-hapus'):?>
        <script type="text/javascript">
                $.toast({
                    heading: 'Success',
                    text: "Produk Berhasil dihapus.",
                    showHideTransition: 'slide',
                    icon: 'success',
                    hideAfter: false,
                    position: 'bottom-right',
                    bgColor: '#7EC857'
                });
        </script>
    <?php else:?>

    <?php endif;?>
</body>
</html>
