<?php $this->load->view('layout/header_penduduk'); ?>

    <?php $this->load->view('layout/sidebar_penduduk'); ?>
    <div class="col-sm-9">
        <?php echo $this->session->flashdata('error');?>
        <div class="card border-radius-10 shadow border-0 mt-4">
        <div class="card-header mt-2">
            <h4>Buat Lapak</h4>
        </div>
        <div class="card-body">
            <div class="container mt-4 mb-4">
            <?php 
                $id_penduduk = $this->session->userdata('id');
                $cek = $this->db->query("SELECT * FROM tbl_toko WHERE id_penduduk='$id_penduduk'");
                if($cek->num_rows() > 0){ ?>
                <center>
                    <img src="<?= base_url('theme/images/not.svg') ?>" class="img img-responsive mb-4" width="40%" alt="">
                    <h4><b>Lapak Sudah Tersedia</b> </h4>
                    <small class="text-muted">Sekarang anda bisa kelola produk anda dengan mudah!</small> <br>
                    <a href="<?= base_url('penduduk/produk/create') ?>" class="btn btn-info btn-lg mt-4">BUAT PRODUK</a>
                </center>
            <?php }else{ ?>
                <form action="<?= base_url('penduduk/toko/store') ?>" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="formGroupExampleInput">Nama Lapak</label>
                        <input type="text" name="nama" class="form-control" id="formGroupExampleInput" placeholder="Masukkan Nama Lapak" required>
                    </div>
                    <div class="form-group">
                        <label for="formGroupExampleInput">Deskripsi Lapak</label>
                        <textarea name="deskripsi" class="form-control" id="" cols="3" rows="3" placeholder="Masukkan Deskripsi Lapak" required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="formGroupExampleInput">Alamat Lapak</label>
                        <textarea name="alamat" class="form-control" id="" cols="3" rows="3" placeholder="Masukkan Alamat Lapak" required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="formGroupExampleInput2">No. Telepon</label>
                        <input type="number" name="no_telepon" class="form-control" id="formGroupExampleInput2" placeholder="Masukkan No. Telepon" required>
                    </div>
                    <div class="form-group">
                        <label for="formGroupExampleInput2">No. Whatsapp</label>
                        <input type="number" class="form-control" id="formGroupExampleInput2" name="no_wa" placeholder="Masukkan No. Whatsapp. Contoh : 6281990123432" required>
                        <small id="passwordHelpBlock" class="form-text mb-3 " style="color: red">
                            *Gunakan awalan 62, Contoh : 6281990123432
                        </small>
                    </div>
                    <div class="form-group">
                        <label for="formGroupExampleInput2">Link Facebook</label>
                        <input type="url" class="form-control" id="formGroupExampleInput2" name="facebook" placeholder="Masukkan Link Facebook">
                    </div>
                    <div class="form-group">
                        <label for="formGroupExampleInput2">Link Instagram</label>
                        <input type="url" class="form-control" id="formGroupExampleInput2" name="instagram" placeholder="Masukkan Link Instagram">
                    </div>
                    <div class="form-group">
                        <label for="formGroupExampleInput2">Logo</label>
                        <input type="file" class="form-control" id="formGroupExampleInput2" name="filefoto" placeholder="Masukkan logo">
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <a href="#" onclick="window.history.back()" class="btn btn-outline-primary">Batal</a>
                </form>
            <?php } ?>
            </div>
        </div>
        </div>
    </div>

<!--//END BLOG -->

<!--============================= FOOTER =============================-->
<?php $this->load->view('layout/footer_penduduk'); ?>