<?php $this->load->view('layout/header_penduduk'); ?>

    <?php $this->load->view('layout/sidebar_penduduk'); ?>
    <div class="col-sm-9">
        <?php echo $this->session->flashdata('message');?>
        <?php echo $this->session->flashdata('error');?>
        <div class="card border-radius-10 shadow border-0 mt-4">
            <div class="card-header mt-2">
                <h4><b>Tambah Foto</b></h4>
            </div>
            <div class="card-body">
                <div class="container mt-4 mb-4">
                    <form action="<?= base_url('penduduk/produk/image_store/') ?>" method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="formGroupExampleInput2">Foto Produk</label>
                            <input type="file" class="form-control" id="formGroupExampleInput2" name="filefoto" placeholder="Masukkan Foto Produk" required>
                            <input type="hidden" name="id_produk" value="<?= $this->uri->segment('4'); ?>">
                        </div>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <a href="#" onclick="window.history.back()" class="btn btn-outline-primary">Batal</a>
                    </form>
                </div>
            </div>
        </div>
        <div class="card border-radius-10 shadow border-0 mt-4">
            <div class="card-header mt-2">
                <h4><b>Daftar Foto</b></h4>
            </div>
            <div class="card-body">
                <div class="container mt-4 mb-4">
                    <div class="table-responsive">
                        <table class="table table-striped" id="display">
                            <thead>
                                <th>No</th>
                                <th>Foto Produk</th>
                                <th>Opsi</th>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach ($produk as $data) : ?>
                                <tr>
                                    <td><?= $no ++; ?></td>
                                    <td>
                                        <img src="<?= base_url('assets/images/produk/slide_produk/') ?><?= $data['foto'] ?>" width="100" alt="">
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-danger btn-circle" data-toggle="modal" data-target="#delete">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!--//END BLOG -->
<!-- Modal -->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body mt-3">
        <center><h3>Peringatan?</h3><p>Apakah yakin data ini dihapus?</p></center>
      </div>
      <div class="modal-footer center">
            <center>
                <form action="<?= base_url('penduduk/produk/image_delete/') ?><?= $data['id'] ?>" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="foto" value="<?= $data['foto'] ?>">
                    <input type="hidden" name="id_produk" value="<?= $this->uri->segment('4')?>">
                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Tidak</button>
                    <button type="submit" class="btn btn-danger">Ya</button>
                </form>
            </center>
      </div>
    </div>
  </div>
</div>
<!--============================= FOOTER =============================-->
<?php $this->load->view('layout/footer_penduduk'); ?>