<?php $this->load->view('layout/header_penduduk'); ?>

    <?php $this->load->view('layout/sidebar_penduduk'); ?>
    <div class="col-sm-9">
        <?php echo $this->session->flashdata('error');?>
        <?php echo $this->session->flashdata('message');?>
        <div class="card border-radius-10 shadow border-0 mt-4">
        <div class="card-header">
            <div class="row p-2">
                <h4 class="mt-2"><b> Produk</b></h4>
                <div class="ml-auto">
                    <a href="<?= base_url('penduduk/produk/create') ?>" class="btn btn-info">BUAT PRODUK</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="container mt-4 mb-4">
            
            <?php 
                $id_penduduk = $this->session->userdata('id');
                $cek = $this->db->query("SELECT * FROM tbl_produk WHERE id_penduduk='$id_penduduk'");
                if($cek->num_rows() > 0){ ?>
                    <div class="table-responsive">
                        <table class="table table-striped" id="display">
                            <thead>
                                <th>No</th>
                                <th style="width: 50%">Nama Produk</th>
                                <th>Status</th>
                                <th>Opsi</th>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach ($produk as $data) : ?>
                                <tr>
                                    <td><?= $no ++; ?></td>
                                    <td><?= $data['produk'] ?></td>
                                    <td><?php if($data['status'] == 'Pending'){echo '<h5><span class="badge badge-danger">Panding</span></h5>';}else{echo $data['status'];} ?></td>
                                    <td>
                                        <a href="<?= base_url('penduduk/produk/image/') ?><?= $data['id'] ?>" class="btn btn-info btn-circle">
                                            <i class="fa fa-image"></i>
                                        </a>
                                        <a href="<?= base_url('penduduk/produk/edit/') ?><?= $data['id'] ?>" class="btn btn-primary btn-circle">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        <button type="button" class="btn btn-danger btn-circle" data-toggle="modal" data-target="#delete">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>    
            <?php }else{ ?>
                <center>
                    <img src="<?= base_url('theme/images/not.svg') ?>" class="img img-responsive mb-4" width="40%" alt="">
                    <h4><b>Produk belum tersedia</b> </h4>
                    <small class="text-muted mb-4">Sekarang anda bisa kelola produk anda dengan mudah!</small> <br>
                    
                </center>
                
            <?php } ?>
            </div>
        </div>
        </div>
    </div>

<!--//END BLOG -->
<!-- Modal -->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body mt-3">
        <center><h3>Peringatan?</h3><p>Apakah yakin data ini dihapus?</p></center>
      </div>
      <div class="modal-footer center">
            <center>
                <form action="<?= base_url('penduduk/produk/delete/') ?><?= $data['id'] ?>" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="foto" value="<?= $data['foto'] ?>">
                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Tidak</button>
                    <button type="submit" class="btn btn-danger">Ya</button>
                </form>
            </center>
      </div>
    </div>
  </div>
</div>
<!--============================= FOOTER =============================-->
<?php $this->load->view('layout/footer_penduduk'); ?>