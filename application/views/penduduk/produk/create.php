<?php $this->load->view('layout/header_penduduk'); ?>

    <?php $this->load->view('layout/sidebar_penduduk'); ?>
    <div class="col-sm-9">
        <?php echo $this->session->flashdata('error');?>
        <div class="card border-radius-10 shadow border-0 mt-4">
        <div class="card-header mt-2">
            <h4><b>Buat Produk</b></h4>
        </div>
        <div class="card-body">
            <div class="container mt-4 mb-4">
                <form action="<?= base_url('penduduk/produk/store') ?>" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="formGroupExampleInput">Nama Produk</label>
                        <input type="text" name="produk" class="form-control" id="formGroupExampleInput" placeholder="Masukkan Nama Produk" required>
                    </div>
                    <div class="row">
                        <div class="col-md-9">
                            <label for="formGroupExampleInput">Harga Produk</label>
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <input type="number" class="form-control" name="harga" placeholder="Masukkan Harga" required onkeypress="return /[0-9]/i.test(event.key)">
                            </div>
                            <small id="passwordHelpBlock" class="form-text mb-3 " style="color: red">
                                *Tanpa tanda titik
                            </small>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="formGroupExampleInput">Satuan Harga</label>
                                <select name="satuan" id="" class="form-control form-control-lg" required>
                                <option value="" disabled="" selected="">-- Pilih --</option>
                                    <option value="pcs">pcs</option>
                                    <option value="paket">paket</option>
                                    <option value="ekor">ekor</option>
                                    <option value="kg">kg</option>
                                    <option value="liter">liter</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="formGroupExampleInput">Alamat</label>
                        <textarea name="alamat" class="form-control" id="" cols="3" rows="3" placeholder="Masukkan Alamat" required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="formGroupExampleInput">Deskripsi</label>
                        <textarea name="deskripsi" class="form-control" id="" cols="3" rows="3" placeholder="Masukkan Deskripsi" required></textarea>
                    </div>
                    
                    <div class="form-group">
                        <label for="formGroupExampleInput2">Foto Produk</label>
                        <input type="file" class="form-control" id="formGroupExampleInput2" name="filefoto" placeholder="Masukkan Foto Produk" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <a href="#" onclick="window.history.back()" class="btn btn-outline-primary">Batal</a>
                </form>
            </div>
        </div>
        </div>
    </div>

<!--//END BLOG -->

<!--============================= FOOTER =============================-->
<?php $this->load->view('layout/footer_penduduk'); ?>