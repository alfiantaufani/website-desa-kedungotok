<?php $this->load->view('layout/header_penduduk'); ?>

    <?php $this->load->view('layout/sidebar_penduduk'); ?>
    <div class="col-sm-9">
        <?php echo $this->session->flashdata('error');?>
        <?php echo $this->session->flashdata('success');?>
        <div class="card border-radius-10 shadow border-0 mt-4">
        <div class="card-header mt-2">
            <h4><b>Edit Profil</b></h4>
        </div>
        <div class="card-body">
            <div class="container mt-4 mb-4">
                <form action="<?= base_url('penduduk/profil/update/') ?><?= $penduduk['id'] ?>" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="formGroupExampleInput">NIK</label>
                        <input type="number" name="nik" class="form-control" id="formGroupExampleInput" value="<?= $penduduk['nik'] ?>" placeholder="Masukkan Nik" required>
                    </div>
                    <div class="form-group">
                        <label for="formGroupExampleInput">Nama</label>
                        <input type="text" name="nama" class="form-control" id="formGroupExampleInput" value="<?= $penduduk['nama'] ?>" placeholder="Masukkan Nama" required>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="formGroupExampleInput">Email</label>
                                <input type="email" name="email" class="form-control" id="formGroupExampleInput" value="<?= $penduduk['email'] ?>" placeholder="Masukkan Email" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="formGroupExampleInput">Jenis Kelamin</label>
                                <select name="jenis_kelamin" id="" class="form-control form-control-lg" required>
                                    <?php if($penduduk['jenis_kelamin'] == 'Laki-laki'){ ?>
                                        <option value="<?= $penduduk['jenis_kelamin'] ?>" selected=""><?= $penduduk['jenis_kelamin'] ?></option>
                                        <option value="Perempuan">Perempuan</option>
                                    <?php }else{ ?>
                                        <option value="<?= $penduduk['jenis_kelamin'] ?>" selected=""><?= $penduduk['jenis_kelamin'] ?></option>
                                        <option value="Laki-laki">Laki-laki</option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="formGroupExampleInput">ALamat</label>
                        <textarea name="alamat" class="form-control" id="" cols="5" rows="5"><?= $penduduk['alamat'] ?></textarea>
                    </div>
                    
                    <div class="form-group">
                        <label for="formGroupExampleInput">No. Telepon</label>
                        <input type="number" name="no_tlp" class="form-control" id="formGroupExampleInput" value="<?= $penduduk['no_tlp'] ?>" placeholder="Masukkan No. Telepon" required>
                    </div>
                    
                    <div class="form-group">
                        <label for="formGroupExampleInput">Password</label>
                        <input type="password" name="password" class="form-control" id="formGroupExampleInput" placeholder="Masukkan Password">
                    </div>
                    
                    <div class="form-group">
                        <label for="formGroupExampleInput">Ulangi Password</label>
                        <input type="password" name="password2" class="form-control" id="formGroupExampleInput" placeholder="Masukkan Ulangi Password">
                    </div>
                    
                    <div class="form-group">
                        <label for="formGroupExampleInput2">Foto</label>
                        <input type="file" class="form-control" id="formGroupExampleInput2" name="filefoto" placeholder="Masukkan Foto profil">
                        <input type="hidden" name="foto" value="<?= $penduduk['foto'] ?>">
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <a href="#" onclick="window.history.back()" class="btn btn-outline-primary">Batal</a>
                </form>
            </div>
        </div>
        </div>
    </div>

<!--//END BLOG -->

<!--============================= FOOTER =============================-->
<?php $this->load->view('layout/footer_penduduk'); ?>