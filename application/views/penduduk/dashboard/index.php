<?php $this->load->view('layout/header_penduduk'); ?>

<?php $this->load->view('layout/sidebar_penduduk'); ?>
<div class="col-sm-9">
  <div class="row">
    <div class="col-12">
      <div class="card border-radius-10 shadow border-0 mt-4 bg-cover">
        <div class="card-header p-3">
          <div class="row">
            <?php 
              $id_penduduk = $this->session->userdata('id');
              $cek = $this->db->query("SELECT * FROM tbl_toko WHERE id_penduduk='$id_penduduk'");
              if($cek->num_rows() > 0){ 
                foreach ($cek->result_array() as $data) : 
            ?>
                <div class="col-sm-4">
                  <div class="d-flex align-items-center">
                    <div class="box-image">
                        <img src="<?= base_url('assets/images/toko/') ?><?= $data['logo']; ?>"  class="rounded-circle img-fluid" width="100%" alt="">
                    </div>
                    <div class="desc-profile ml-3">
                        <h4 class="text-white">
                          <b>
                            <?php 
                              if(strlen($data['nama']) > 15){
                                echo substr($data['nama'], 0, strpos(wordwrap($data['nama'], 15), "\n"));
                                echo "...";
                              }else{
                                echo $data['nama'];
                              }
                           ?>
                          </b>
                        </h4>
                        <small class="text-white">
                          <?php 
                              if(strlen($data['alamat']) > 25 ){
                                echo substr($data['alamat'], 0, strpos(wordwrap($data['alamat'], 25 ), "\n"));
                                echo "...";
                              }else{
                                echo $data['alamat'];
                              }
                           ?>
                        </small>
                    </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="card border-radius-10 shadow border-0">
                    <div class="card-body">
                      <div class="container mt-4">
                        <div class="col-sm-12">
                          <a href="#" class="text-decoration-none text-dark">
                            <h3 class="font-weight-bold"><?= $pending ?> Unit</h3>
                          </a>
                          <p class="mt-2 text-muted">Produk <span style="color: #ec3031;">(Pending)</span></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="card border-radius-10 shadow border-0">
                    <div class="card-body">
                      <div class="container mt-4">
                        <div class="col-sm-12">
                          <a href="#" class="text-decoration-none text-dark">
                            <h3 class="font-weight-bold"><?= $aktif ?> Unit</h3>
                          </a>
                          <p class="mt-2 text-muted">Produk <span style="color: #0275d8;">(Aktif)</span></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            <?php endforeach; }else{ ?>
                <div class="col-sm-6">
                  <div class="card border-radius-10 shadow border-0">
                    <div class="card-body">
                      <div class="container mt-4">
                        <div class="col-sm-12">
                          <a href="#" class="text-decoration-none text-dark">
                            <h3 class="font-weight-bold">0 Unit</h3>
                          </a>
                          <p class="mt-2 text-muted">Produk <span style="color: #ec3031;">(Pending)</span></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="card border-radius-10 shadow border-0">
                    <div class="card-body">
                      <div class="container mt-4">
                        <div class="col-sm-12">
                          <a href="#" class="text-decoration-none text-dark">
                            <h3 class="font-weight-bold">0 Unit</h3>
                          </a>
                          <p class="mt-2 text-muted">Produk <span style="color: #0275d8;">(Aktif)</span></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container row">
      <h4 class="font-weight-bold mt-4">Produk Populer</h4>
      <div class="ml-auto mt-4">
          <a href="<?= base_url('penduduk/produk') ?>" class="text-theme text-decoration-none">Lihat semua <i class="fa fa-angle-right"></i></a>
      </div>
  </div>
  <div class="card border-radius-10 shadow border-0 mt-4">
    <div class="card-body">
      <div class="container mt-4">
        <div class="table-responsive">
          <table class="table table-striped" id="display">
            <tbody>
              <?php foreach ($produk_populer as $data) : ?>
                <tr>
                  <td><?= $data['produk'] ?></td>
                  <td><?= $data['view'] ?> view</td>
                </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="container row">
      <h4 class="font-weight-bold mt-4">Produk Aktif</h4>
      <div class="ml-auto mt-4">
          <a href="<?= base_url('penduduk/produk') ?>" class="text-theme text-decoration-none">Lihat semua <i class="fa fa-angle-right"></i></a>
      </div>
  </div>
  <div class="row mt-3">
    <?php foreach ($produk_aktif as $data) : ?>
    <div class="col-md-6">
      <div class="product-card mb-4 overflow-hidden d-lg-flex flex-column position-relative border-radius-7">
        <div class="product-image overflow-hidden">
          <img src="<?= base_url('assets/images/produk/') ?><?= $data['foto']?>" alt="" class="product-thumbnail">
        </div>
        <div class="p-4 product-details">
          <h4 class="font-weight-bold d-flex justify-content-between">
            <a href="<?= base_url('produk/') ?><?= $data['slug']?>" target="_blank" class="text-dark mb-2 mr-3" title="<?= $data['produk']?>">
            <?php 
              if(strlen($data['produk']) > 35){
                echo substr($data['produk'], 0, strpos(wordwrap($data['produk'], 35), "\n"));
                echo "...";
              }else{
                echo $data['produk'];
              }
            ?>
            </a>
          </h4>
          <div class="d-flex align-items-baseline">
            <h3 class="font-weight-bold mr-2 mt-2">Rp. <?= number_format($data['harga'])?></h3><h5 class="text-striked text-muted mr-3 font-weight-regular">/<?= $data['satuan_harga']?></h5>
          </div>
          <small>
            <?php 
              if(strlen($data['deskripsi']) > 50 ){
                echo substr($data['deskripsi'], 0, strpos(wordwrap($data['deskripsi'], 50), "\n"));
                echo "...";
              }else{
                echo $data['deskripsi'];
              }
            ?>
          </small>
          <div class="produk-footer d-flex justify-content-between mb-2 mt-3 pt-2">
            <div>
              <img src="<?= base_url('assets/images/toko/') ?><?= $data['logo']; ?>"  class="produk-author rounded-circle img-fluid shadow-sm" alt="">
              <a href="<?= base_url('toko/') ?><?= $data['slug_toko']?>" class="text-dark">
                <?php 
                  if(strlen($data['nama']) > 20){
                    echo substr($data['nama'], 0, strpos(wordwrap($data['nama'], 20), "\n"));
                    echo "...";
                  }else{
                    echo $data['nama'];
                  }
                ?>
              </a>
            </div>
            <small class="text-muted pt-2"><?php echo date("d M Y", strtotime($data['created_at']));?> | Dilihat <?= $data['view']; ?>x</small>
          </div>
        </div>
      </div>
    </div>
    <?php endforeach ?>
  </div>
</div>

<!--============================= FOOTER =============================-->
<?php $this->load->view('layout/footer_penduduk'); ?>