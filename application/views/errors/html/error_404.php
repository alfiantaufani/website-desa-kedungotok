<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>404 Page Not Found</title>
<style type="text/css">

::selection { background-color: #E13300; color: white; }
::-moz-selection { background-color: #E13300; color: white; }

body {
	background-color: #fff;
	margin: 40px;
	font: 13px/20px normal Helvetica, Arial, sans-serif;
	color: #4F5155;
}

a {
	color: #003399;
	background-color: transparent;
	font-weight: normal;
}

h1 {
	color: #444;
	background-color: transparent;
	font-size: 24px;
	font-weight: bold;
	margin: 10px 0 19px 0;
	padding: 60px 15px 20px 15px;
}

code {
	font-family: Consolas, Monaco, Courier New, Courier, monospace;
	font-size: 12px;
	background-color: #f9f9f9;
	border: 1px solid #D0D0D0;
	color: #002166;
	display: block;
	margin: 14px 0 14px 0;
	padding: 12px 10px 12px 10px;
}

#container {
	margin: 10px;
	height: 400px;
	border-radius: 0.5rem;
	box-shadow: -1px 3px 10px 0 rgb(0 0 0 / 6%);
}

p {
	margin: 12px 15px 12px 15px;
}

a{
	padding: 10px 40px 10px;
	background-color: #1cb9c8;
    color: #ffffff !important;
    border: 2px
 	solid #1cb9c8;
	text-decoration: none;
}
a:hover{
	background: #1aa7b5 none repeat scroll 0 0;
    border: 2px solid #1cb9c8;
}
</style>
</head>
<body>
	<div id="container">
		<center>
			<h1>
				<img src="http://website-desa-v3.s3-id-jkt-1.kilatstorage.id/logo_homepage/selamat-datang-di-situs-resmi-desa-contoh.png" alt="" width="10%"> <br><br>
				404 <br><br> Page Not Found
			</h1>
			<?php echo $message; ?>
			<a href="#" onclick="back()">Kembali</a>
		</center> 
	</div>
	<script>
		function back() {
			window.history.back();
		}
	</script>
</body>
</html>