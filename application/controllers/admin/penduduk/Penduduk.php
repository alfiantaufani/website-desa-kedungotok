<?php
class Penduduk extends CI_Controller{
    function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('login');
            redirect($url);
        };
		//$this->load->model('m_pages');
		$this->load->library('upload');
	}

    public function index()
    {
        $data['penduduk'] = $this->db->query("SELECT * FROM tbl_penduduk ORDER BY id DESC");
		$data['title'] = "Data Penduduk";
		$this->load->view('admin/penduduk/index',$data);
    }

    public function update($id)
    {
        $password = $this->input->post('password');
        $password2 = $this->input->post('password2');
        if($password == "" || $password2 == ""){ // pass kosong, ada & tiada gambar
            $config['upload_path'] = './assets/images/penduduk/'; 
            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; 
            $config['encrypt_name'] = TRUE;
            $this->upload->initialize($config);
            if(!empty($_FILES['filefoto']['name'])){
                if ($this->upload->do_upload('filefoto')){
                    $gbr = $this->upload->data();
                        $cek = $this->input->post('gambar');
                        $path='./assets/images/penduduk/'.$cek;
                        unlink($path);
                    $gambar = $gbr['file_name'];
                    $data = [
                        'nik' => $this->input->post('nik'),
                        'nama' => $this->input->post('nama'),
                        'email' => $this->input->post('email'),
                        'jenis_kelamin' => $this->input->post('jenkel'),
                        
                        'no_tlp' => $this->input->post('no_tlp'),
                        'foto' => $gambar,
                    ];
                    $this->db->where('id', $id);
                    $update = $this->db->update("tbl_penduduk", $data);
                }
            }else{
                $data = [
                    'nik' => $this->input->post('nik'),
                    'nama' => $this->input->post('nama'),
                    'email' => $this->input->post('email'),
                    'jenis_kelamin' => $this->input->post('jenkel'),
                    
                    'no_tlp' => $this->input->post('no_tlp'),
                ];
                $this->db->where('id', $id);
                $update = $this->db->update("tbl_penduduk", $data);
            }
        }else{
            if($password == $password2){
                $config['upload_path'] = './assets/images/penduduk/'; 
                $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; 
                $config['encrypt_name'] = TRUE;
                $this->upload->initialize($config);
                if(!empty($_FILES['filefoto']['name']))
                {
                    if ($this->upload->do_upload('filefoto'))
                    {
                        $gbr = $this->upload->data();
                            $cek = $this->input->post('foto');
                            $path='./assets/images/penduduk/'.$cek;
                            unlink($path);
                        $gambar = $gbr['file_name'];
                        $data = [
                            'nik' => $this->input->post('nik'),
                            'nama' => $this->input->post('nama'),
                            'email' => $this->input->post('email'),
                            'password' => md5($password2),
                            'jenis_kelamin' => $this->input->post('jenkel'),
                            'no_tlp' => $this->input->post('no_tlp'),
                            'foto' => $gambar,
                        ];
                        $this->db->where('id', $id);
                        $update = $this->db->update("tbl_penduduk", $data);
                    }
                }else{
                    $data = [
                        'nik' => $this->input->post('nik'),
                        'nama' => $this->input->post('nama'),
                        'email' => $this->input->post('email'),
                        'password' => md5($password2),
                        'jenis_kelamin' => $this->input->post('jenkel'),
                        
                        'no_tlp' => $this->input->post('no_tlp'),
                    ];
                    $this->db->where('id', $id);
                    $update = $this->db->update("tbl_penduduk", $data);
                }
            }else{
                echo $this->session->set_flashdata('msg','error-password');
                redirect('admin/penduduk/penduduk');
            }
        }

        if($update){
            echo $this->session->set_flashdata('msg','info');
            redirect('admin/penduduk/penduduk');  
        }else{
            echo $this->session->set_flashdata('msg','error');
            redirect('admin/penduduk/penduduk');
        }
    }

    public function delete($id)
    {
        echo $this->session->set_flashdata('msg','error-delete');
        redirect('admin/penduduk/penduduk');
		// $gambar = $this->input->post('gambar');
		// $path = './assets/images/penduduk/'.$gambar;
		// unlink($path);
		// $hsl	= $this->db->query("DELETE FROM tbl_penduduk WHERE id='$id'");
		// if($hsl){
		// 	echo $this->session->set_flashdata('msg','success-hapus');
		// 	redirect('admin/penduduk/penduduk');
		// }else{
		// 	echo $this->session->set_flashdata('msg','error');
        //     redirect('admin/penduduk/penduduk');
		// }
    }
}