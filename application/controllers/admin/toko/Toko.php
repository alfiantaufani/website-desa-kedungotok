<?php
class Toko extends CI_Controller{
    function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('login');
            redirect($url);
        };
		//$this->load->model('m_pages');
		$this->load->library('upload');
	}

    public function index()
    {
        $data['toko'] = $this->db->query("SELECT tbl_toko.id, tbl_toko.nama, tbl_toko.slug, tbl_toko.no_telepon, tbl_toko.no_wa, tbl_toko.logo, tbl_penduduk.nama AS nama_pemilik FROM tbl_toko INNER JOIN tbl_penduduk ON tbl_toko.id_penduduk=tbl_penduduk.id ORDER BY tbl_toko.id DESC");
		$data['title'] = "Data Lapak Penduduk";
		$this->load->view('admin/toko/index',$data);
    }

    public function delete($id)
    {
        $cek_produk = $this->db->query("SELECT id_toko, foto FROM tbl_produk WHERE id_toko='$id'");
        if($cek_produk->num_rows() > 0){
            $foto_produk = $cek_produk->row_array();
            $path = './assets/images/produk/'.$foto_produk['foto'];
		    unlink($path);
            $delete	= $this->db->query("DELETE FROM tbl_produk WHERE id_toko='$id'");
            if($delete){
                $gambar = $this->input->post('gambar');
                $path = './assets/images/toko/'.$gambar;
                unlink($path);
                $hsl	= $this->db->query("DELETE FROM tbl_toko WHERE id='$id'");
                if($hsl){
                    echo $this->session->set_flashdata('msg','success-hapus');
                    redirect('admin/toko/toko');
                }else{
                    echo $this->session->set_flashdata('msg','error');
                    redirect('admin/toko/toko');
                }
            }else{
                echo $this->session->set_flashdata('msg','error-delete');
                redirect('admin/toko/toko');
            }
        }else{
            $gambar = $this->input->post('gambar');
            $path = './assets/images/toko/'.$gambar;
            unlink($path);
            $hsl	= $this->db->query("DELETE FROM tbl_toko WHERE id='$id'");
            if($hsl){
                echo $this->session->set_flashdata('msg','success-hapus');
                redirect('admin/toko/toko');
            }else{
                echo $this->session->set_flashdata('msg','error');
                redirect('admin/toko/toko');
            }
        }
    }
}