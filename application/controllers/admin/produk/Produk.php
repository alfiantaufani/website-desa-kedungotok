<?php
class Produk extends CI_Controller{
    function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('login');
            redirect($url);
        };
	}

    public function index()
    {
        $data['produk'] = $this->db->query("SELECT tbl_produk.id AS id_produk, tbl_produk.id_penduduk, tbl_produk.id_toko, tbl_produk.produk, tbl_produk.harga, tbl_produk.satuan_harga, tbl_produk.slug, tbl_produk.alamat, tbl_produk.foto, tbl_produk.deskripsi, tbl_produk.status, tbl_produk.created_at, tbl_toko.nama, tbl_toko.logo, tbl_toko.slug AS slug_toko, tbl_toko.no_telepon, tbl_toko.no_wa, tbl_toko.facebook, tbl_toko.instagram FROM tbl_produk INNER JOIN tbl_toko ON tbl_produk.id_toko=tbl_toko.id ORDER BY tbl_produk.id DESC");
		$data['title'] = "Data Produk Penduduk";
		$this->load->view('admin/produk/index',$data);
    }

    public function update($id)
    {
        $data = [
            'status' => $this->input->post('status')
        ];
        $this->db->where('id', $id);
        $update = $this->db->update("tbl_produk", $data);

        if($update){
            echo $this->session->set_flashdata('msg','info');
            redirect('admin/produk/produk');  
        }else{
            echo $this->session->set_flashdata('msg','error');
            redirect('admin/produk/produk');
        }
    }
}