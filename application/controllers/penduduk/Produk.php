<?php
class Produk extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('penduduk_masuk') != TRUE){
            $url = base_url('login');
            redirect($url);
        }
        $this->load->library('upload');
	}
	public function index(){
        $id_penduduk = $this->session->userdata('id');
		$data['title'] = "Produk";
        $data['produk'] = $this->db->query("SELECT * FROM tbl_produk WHERE id_penduduk='$id_penduduk' ORDER BY id DESC")->result_array();
		$this->load->view('penduduk/produk/index',$data);
	}

    public function create()
    {
        $data['title'] = "Tambah Produk";
        $this->load->view('penduduk/produk/create',$data);
    }

    public function store()
    {
        $produk		= strip_tags($this->input->post('produk'));
		$slug       = url_title($produk, '-', TRUE);

        $id_penduduk = $this->session->userdata('id');
        $cek_toko = $this->db->query("SELECT * FROM tbl_toko WHERE id_penduduk='$id_penduduk'");

        if($cek_toko->num_rows() > 0){
            $id_toko = $cek_toko->row_array();
            $cek_slug = $this->db->query("SELECT * FROM tbl_produk WHERE slug='$slug' LIMIT 1");
            if($cek_slug->num_rows() > 0){
                echo $this->session->set_flashdata('error', '<div class="alert alert-danger" role="alert">
                        <b>Gagal!</b> Nama produk sudah ada. <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
                    ');
                redirect('penduduk/produk/create');
            }else{
                $config['upload_path'] = './assets/images/produk/'; //path folder
                $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
                $config['encrypt_name'] = TRUE; //nama yang terupload nantinya
        
                $this->upload->initialize($config);
                if(!empty($_FILES['filefoto']['name']))
                {
                    if ($this->upload->do_upload('filefoto'))
                    {
                        $gbr = $this->upload->data();
                        
                        $gambar = $gbr['file_name'];
                        $data = [
                            'id_penduduk' => $this->session->userdata('id'),
                            'id_toko' => $id_toko['id'],
                            'produk' => $this->input->post('produk'),
                            'harga' => $this->input->post('harga'),
                            'satuan_harga' => $this->input->post('satuan'),
                            'slug' => $slug,
                            'alamat' => $this->input->post('alamat'),
                            'foto' => $gambar,
                            'deskripsi' => $this->input->post('deskripsi'),
                            'view' => '0',
                            'status' => 'Pending',
                        ];
                        $insert = $this->db->insert("tbl_produk", $data);
                    }
                }else{
                    $data = [
                        'id_penduduk' => $this->session->userdata('id'),
                        'id_toko' => $id_toko['id'],
                        'produk' => $this->input->post('produk'),
                        'harga' => $this->input->post('harga'),
                        'satuan_harga' => $this->input->post('satuan'),
                        'slug' => $slug,
                        'alamat' => $this->input->post('alamat'),
                        'deskripsi' => $this->input->post('deskripsi'),
                        'view' => '0',
                        'status' => 'Pending',
                    ];
                    $insert = $this->db->insert("tbl_produk", $data);
                }
    
                if($insert){
                    echo $this->session->set_flashdata('message', '<div class="alert alert-info" role="alert">
                            <b>Berhasil!</b> Produk berhasil di tambah. <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
                        ');
                    redirect('penduduk/produk');  
                }else{
                    echo $this->session->set_flashdata('error', '<div class="alert alert-danger" role="alert">
                            <b>Gagal!</b> Produk gagal di tambah. <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
                        ');
                    redirect('penduduk/produk');
                }
            }
        }else{
            echo $this->session->set_flashdata('error', '<div class="alert alert-danger" role="alert">
                    <b>Gagal!</b> Lapak belum tersedia. <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
                ');
            redirect('penduduk/produk/create');
        }

    }

    public function edit($id)
    {
        $data['title'] = "Edit Produk";
        $data['produk'] = $this->db->query("SELECT * FROM tbl_produk WHERE id='$id'")->row_array();
		$this->load->view('penduduk/produk/edit',$data);
    }

    public function update($id)
    {
        $config['upload_path'] = './assets/images/produk/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                $gbr = $this->upload->data();
                $cek = $this->input->post('foto');
                $path = './assets/images/produk/'.$cek;
                unlink($path);
                $gambar = $gbr['file_name'];
                $data = [
                    'id_penduduk' => $this->session->userdata('id'),
                    'produk' => $this->input->post('produk'),
                    'harga' => $this->input->post('harga'),
                    'satuan_harga' => $this->input->post('satuan'),
                    'alamat' => $this->input->post('alamat'),
                    'foto' => $gambar,
                    'deskripsi' => $this->input->post('deskripsi'),
                ];
                $this->db->where('id', $id);
                $update = $this->db->update("tbl_produk", $data);
            }
        }else{
            $data = [
                'id_penduduk' => $this->session->userdata('id'),
                'produk' => $this->input->post('produk'),
                'harga' => $this->input->post('harga'),
                'satuan_harga' => $this->input->post('satuan'),
                'alamat' => $this->input->post('alamat'),
                'deskripsi' => $this->input->post('deskripsi'),
            ];
            $this->db->where('id', $id);
            $update = $this->db->update("tbl_produk", $data);
        }

        if($update){
            echo $this->session->set_flashdata('message', '<div class="alert alert-info" role="alert">
                    <b>Berhasil!</b> Produk berhasil di update. <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
                ');
            redirect('penduduk/produk');  
        }else{
            echo $this->session->set_flashdata('error', '<div class="alert alert-danger" role="alert">
                    <b>Gagal!</b> Produk gagal di update. <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
                ');
            redirect('penduduk/produk');
        }
    }
    
    public function delete($id)
    {
        $cek_foto = $this->db->query("SELECT * FROM tbl_foto_produk WHERE id_produk='$id'");
        if($cek_foto->num_rows() > 0){
            // delete foto slide produk
            $data_foto = $cek_foto->row_array();
            $foto_slide = $data_foto['foto'];
            $path = './assets/images/produk/slide_produk/'.$foto_slide;
            unlink($path);
            $this->db->query("DELETE FROM tbl_foto_produk WHERE id_produk='$id'");

            // delete produk & fotonya
            $foto = $this->input->post('foto');
            $path = './assets/images/produk/'.$foto;
            unlink($path);
            $hsl	= $this->db->query("DELETE FROM tbl_produk WHERE id='$id'");
        }else{
            $foto = $this->input->post('foto');
            $path = './assets/images/produk/'.$foto;
            unlink($path);
            $hsl	= $this->db->query("DELETE FROM tbl_produk WHERE id='$id'");
        }    
            if($hsl){
                echo $this->session->set_flashdata('message', '<div class="alert alert-info" role="alert">
                        <b>Berhasil!</b> Produk berhasil di hapus. <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
                    ');
                redirect('penduduk/produk');
            }else{
                echo $this->session->set_flashdata('error', '<div class="alert alert-danger" role="alert">
                        <b>Gagal!</b> Produk gagal di hapus. <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
                    ');
                redirect('penduduk/produk');
            }
        

    }

    public function image($id)
    {
        $data['title'] = "Tambah Foto";
        $data['produk'] = $this->db->query("SELECT * FROM tbl_foto_produk WHERE id_produk='$id'")->result_array();
		$this->load->view('penduduk/produk/image',$data);
    }

    public function image_store()
    {
        $id_produk = $this->input->post('id_produk');
        $config['upload_path'] = './assets/images/produk/slide_produk/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                $gbr = $this->upload->data();
                
                $gambar = $gbr['file_name'];
                $data = [
                    'id_produk' => $this->input->post('id_produk'),
                    'foto' => $gambar,
                ];
                $insert = $this->db->insert("tbl_foto_produk", $data);
            }
        }

        if($insert){
            echo $this->session->set_flashdata('message', '<div class="alert alert-info" role="alert">
                    <b>Berhasil!</b> Produk berhasil di tambah. <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
                ');
                redirect("penduduk/produk/image/$id_produk"); 
        }else{
            echo $this->session->set_flashdata('error', '<div class="alert alert-danger" role="alert">
                    <b>Gagal!</b> Produk gagal di tambah. <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
                ');
                redirect("penduduk/produk/image/$id_produk");
        }
    }

    public function image_delete($id)
    {
        $foto = $this->input->post('foto');
        $id_produk = $this->input->post('id_produk');
		$path = './assets/images/produk/slide_produk/'.$foto;
		unlink($path);
		$hsl	= $this->db->query("DELETE FROM tbl_foto_produk WHERE id='$id'");
		if($hsl){
			echo $this->session->set_flashdata('message', '<div class="alert alert-info" role="alert">
                    <b>Berhasil!</b> Produk berhasil di hapus. <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
                ');
			redirect("penduduk/produk/image/$id_produk");
		}else{
			echo $this->session->set_flashdata('error', '<div class="alert alert-danger" role="alert">
                    <b>Gagal!</b> Produk gagal di hapus. <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
                ');
            redirect("penduduk/produk/image/$id_produk");
		}
    }
}