<?php
class Profil extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('penduduk_masuk') != TRUE){
            $url = base_url('login');
            redirect($url);
        }
        $this->load->library('upload');
	}

    public function index()
    {
        $id = $this->session->userdata('id');
		$data['title'] = "Profil";
        $data['penduduk'] = $this->db->query("SELECT * FROM tbl_penduduk WHERE id='$id' LIMIT 1")->row_array();
		$this->load->view('penduduk/profil/index',$data);
    }

    public function update($id)
    {
        $password = $this->input->post('password');
        $password2 = $this->input->post('password2');
        if($password == "" || $password2 == ""){ // pass kosong, ada & tiada gambar
            $config['upload_path'] = './assets/images/penduduk/'; 
            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; 
            $config['encrypt_name'] = TRUE;
            $this->upload->initialize($config);
            if(!empty($_FILES['filefoto']['name'])){
                if ($this->upload->do_upload('filefoto')){
                    $gbr = $this->upload->data();
                        $cek = $this->input->post('foto');
                        $path='./assets/images/penduduk/'.$cek;
                        unlink($path);
                    $gambar = $gbr['file_name'];
                    $data = [
                        'nik' => $this->input->post('nik'),
                        'nama' => $this->input->post('nama'),
                        'email' => $this->input->post('email'),
                        'jenis_kelamin' => $this->input->post('jenis_kelamin'),
                        'alamat' => $this->input->post('alamat'),
                        'no_tlp' => $this->input->post('no_tlp'),
                        'foto' => $gambar,
                    ];
                    $this->db->where('id', $id);
                    $update = $this->db->update("tbl_penduduk", $data);
                }
            }else{
                $data = [
                    'nik' => $this->input->post('nik'),
                    'nama' => $this->input->post('nama'),
                    'email' => $this->input->post('email'),
                    'jenis_kelamin' => $this->input->post('jenis_kelamin'),
                    'alamat' => $this->input->post('alamat'),
                    'no_tlp' => $this->input->post('no_tlp'),
                ];
                $this->db->where('id', $id);
                $update = $this->db->update("tbl_penduduk", $data);
            }
        }else{
            if($password == $password2){
                $config['upload_path'] = './assets/images/penduduk/'; 
                $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; 
                $config['encrypt_name'] = TRUE;
                $this->upload->initialize($config);
                if(!empty($_FILES['filefoto']['name']))
                {
                    if ($this->upload->do_upload('filefoto'))
                    {
                        $gbr = $this->upload->data();
                            $cek = $this->input->post('foto');
                            $path='./assets/images/penduduk/'.$cek;
                            unlink($path);
                        $gambar = $gbr['file_name'];
                        $data = [
                            'nik' => $this->input->post('nik'),
                            'nama' => $this->input->post('nama'),
                            'email' => $this->input->post('email'),
                            'password' => md5($password2),
                            'jenis_kelamin' => $this->input->post('jenis_kelamin'),
                            'alamat' => $this->input->post('alamat'),
                            'no_tlp' => $this->input->post('no_tlp'),
                            'foto' => $gambar,
                        ];
                        $this->db->where('id', $id);
                        $update = $this->db->update("tbl_penduduk", $data);
                    }
                }else{
                    $data = [
                        'nik' => $this->input->post('nik'),
                        'nama' => $this->input->post('nama'),
                        'email' => $this->input->post('email'),
                        'password' => md5($password2),
                        'jenis_kelamin' => $this->input->post('jenis_kelamin'),
                        'alamat' => $this->input->post('alamat'),
                        'no_tlp' => $this->input->post('no_tlp'),
                    ];
                    $this->db->where('id', $id);
                    $update = $this->db->update("tbl_penduduk", $data);
                }
            }else{
                echo $this->session->set_flashdata('error','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Password Tidak sama</div>');
                redirect('penduduk/profil');
            }
        }

        if($update){
            echo $this->session->set_flashdata
                ('success', '<div class="alert alert-info" role="alert">
                    <b>Berhasil!</b> Profil berhasil di update. <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
                ');
            redirect('penduduk/profil');  
        }else{
            echo $this->session->set_flashdata
                ('error', '<div class="alert alert-danger" role="alert">
                    <b>Gagal!</b> Profil gagal di update. <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
                ');
            redirect('penduduk/profil');
        }
    }
}