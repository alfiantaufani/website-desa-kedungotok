<?php
class Dashboard extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('penduduk_masuk') != TRUE){
            $url = base_url('login');
            redirect($url);
        }
	}
	public function index(){
		$id_penduduk = $this->session->userdata('id');
		$data['title'] = "Dashboard";

        $data['pending'] = $this->db->query("SELECT * FROM tbl_produk WHERE id_penduduk='$id_penduduk' AND status='Pending'")->num_rows();

        $data['aktif'] = $this->db->query("SELECT * FROM tbl_produk WHERE id_penduduk='$id_penduduk' AND status='Aktif'")->num_rows();

		$data['produk_aktif'] = $this->db->query("SELECT tbl_produk.id_toko, tbl_produk.produk, tbl_produk.harga, tbl_produk.satuan_harga, tbl_produk.slug, tbl_produk.alamat, tbl_produk.foto, tbl_produk.deskripsi, tbl_produk.view, tbl_produk.created_at, tbl_toko.nama, tbl_toko.logo, tbl_toko.slug AS slug_toko, tbl_toko.no_telepon, tbl_toko.no_wa, tbl_toko.facebook, tbl_toko.instagram FROM tbl_produk INNER JOIN tbl_toko ON tbl_produk.id_toko=tbl_toko.id WHERE tbl_produk.id_penduduk='$id_penduduk' AND tbl_produk.status='Aktif' ORDER BY tbl_produk.id DESC")->result_array();

		$data['produk_populer'] = $this->db->query("SELECT * FROM tbl_produk INNER JOIN tbl_toko ON tbl_produk.id_toko=tbl_toko.id WHERE tbl_produk.id_penduduk='$id_penduduk' AND tbl_produk.status='Aktif' ORDER BY tbl_produk.view DESC LIMIT 3")->result_array();

		$this->load->view('penduduk/dashboard/index',$data);
	}
	
}