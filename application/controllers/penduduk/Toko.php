<?php
class Toko extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('penduduk_masuk') != TRUE){
            $url = base_url('login');
            redirect($url);
        }
        $this->load->library('upload');
	}
	public function index(){
		$data['title'] = "Kelola Lapak";
		$this->load->view('penduduk/toko/index',$data);
	}

    public function create()
    {
        $data['title'] = "Buat Lapak";
		$this->load->view('penduduk/toko/create',$data);
    }

    public function store()
    {
        $nama		= strip_tags($this->input->post('nama'));
		$slug       = url_title($nama, '-', TRUE);

        $cek_slug = $this->db->query("SELECT * FROM tbl_toko WHERE slug='$slug' LIMIT 1");
		if($cek_slug->num_rows() > 0){
			echo $this->session->set_flashdata
                ('error', '<div class="alert alert-danger" role="alert">
                    <b>Gagal!</b> Nama Lapak sudah ada. <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
                ');
            redirect('penduduk/toko/create');
		}else{
            $config['upload_path'] = './assets/images/toko/'; //path folder
            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
            $config['encrypt_name'] = TRUE; //nama yang terupload nantinya
    
            $this->upload->initialize($config);
            if(!empty($_FILES['filefoto']['name']))
            {
                if ($this->upload->do_upload('filefoto'))
                {
                    $gbr = $this->upload->data();
                    
                    $gambar = $gbr['file_name'];
                    $data = [
                        'id_penduduk' => $this->session->userdata('id'),
                        'nama' => $this->input->post('nama'),
                        'deskripsi' => $this->input->post('deskripsi'),
                        'alamat' => $this->input->post('alamat'),
                        'logo' => $gambar,
                        'slug' => $slug, 
                        'no_telepon' => $this->input->post('no_telepon'),
                        'no_wa' => $this->input->post('no_wa'),
                        'facebook' => $this->input->post('facebook'),
                        'instagram' => $this->input->post('instagram'),
                    ];
                    $insert = $this->db->insert("tbl_toko", $data);
                    
                }
            }else{
                $data = [
                    'id_penduduk' => $this->session->userdata('id'),
                    'nama' => $this->input->post('nama'),
                    'deskripsi' => $this->input->post('deskripsi'),
                    'alamat' => $this->input->post('alamat'),
                    'slug' => $slug, 
                    'no_telepon' => $this->input->post('no_telepon'),
                    'no_wa' => $this->input->post('no_wa'),
                    'facebook' => $this->input->post('facebook'),
                    'instagram' => $this->input->post('instagram'),
                ];
                $insert = $this->db->insert("tbl_toko", $data);
            }

            if($insert){
                echo $this->session->set_flashdata
                    ('message', '<div class="alert alert-info" role="alert">
                        <b>Berhasil!</b> Lapak berhasil di update. <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
                    ');
                redirect('penduduk/toko');  
            }else{
                echo $this->session->set_flashdata
                    ('error', '<div class="alert alert-danger" role="alert">
                        <b>Gagal!</b> Lapak gagal di update. <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
                    ');
                redirect('penduduk/toko');
            }
        }

    }

    public function update()
    {
        $config['upload_path'] = './assets/images/toko/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                $gbr = $this->upload->data();
                $gambar = $gbr['file_name'];
                    $cek = $this->input->post('logo');
                    $path='./assets/images/toko/'.$cek;
                    unlink($path);
                $data = [
                    'id_penduduk' => $this->session->userdata('id'),
                    'nama' => $this->input->post('nama'),
                    'deskripsi' => $this->input->post('deskripsi'),
                    'alamat' => $this->input->post('alamat'),
                    'logo' => $gambar,
                    'no_telepon' => $this->input->post('no_telepon'),
                    'no_wa' => $this->input->post('no_wa'),
                    'facebook' => $this->input->post('facebook'),
                    'instagram' => $this->input->post('instagram'),
                ];
                
                $this->db->where('id', $this->input->post('id'));
                $update = $this->db->update("tbl_toko", $data);
            }else{
                echo $this->session->set_flashdata
                    ('error', '<div class="alert alert-danger" role="alert">
                        <b>Gagal!</b> Lapak gagal di update. <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
                    ');
                redirect('penduduk/toko');
            }
        }else{
            $data = [
                'id_penduduk' => $this->session->userdata('id'),
                'nama' => $this->input->post('nama'),
                'deskripsi' => $this->input->post('deskripsi'),
                'alamat' => $this->input->post('alamat'),
                'no_telepon' => $this->input->post('no_telepon'),
                'no_wa' => $this->input->post('no_wa'),
                'facebook' => $this->input->post('facebook'),
                'instagram' => $this->input->post('instagram'),
            ];
            $this->db->where('id', $this->input->post('id'));
            $update = $this->db->update("tbl_toko", $data);
        }

        if($update){
            echo $this->session->set_flashdata
                ('message', '<div class="alert alert-info" role="alert">
                    <b>Berhasil!</b> Lapak berhasil di update. <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
                ');
            redirect('penduduk/toko');  
        }else{
            echo $this->session->set_flashdata
                ('error', '<div class="alert alert-danger" role="alert">
                    <b>Gagal!</b> Lapak gagal di update. <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
                ');
            redirect('penduduk/toko');
        }
    }
	
}