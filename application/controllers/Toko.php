<?php
class Toko extends CI_Controller{
    function __construct(){
		parent::__construct();
		//$this->load->model('m_toko');
	}

    public function index()
    {
        return show_404();
    }

    public function detail($slugs)
    {
        $slug   = htmlspecialchars($slugs,ENT_QUOTES);
		$data['title'] = "Lapak Desa";
        $query = $this->db->query("SELECT slug FROM tbl_toko WHERE slug='$slug'");

        if($query->num_rows() > 0){
            $data['toko'] = $this->db->query("SELECT tbl_toko.id_penduduk, tbl_toko.nama AS nama_toko, tbl_toko.deskripsi, tbl_toko.alamat AS alamat_toko, tbl_toko.logo, tbl_toko.no_telepon, tbl_toko.no_wa, tbl_toko.facebook, tbl_toko.instagram, tbl_penduduk.nama AS nama_penduduk FROM tbl_toko INNER JOIN tbl_penduduk ON tbl_toko.id_penduduk=tbl_penduduk.id WHERE tbl_toko.slug='$slug'")->row_array();

            $data['produk_populer'] = $this->db->query("SELECT tbl_produk.id_toko, tbl_produk.produk, tbl_produk.harga, tbl_produk.satuan_harga, tbl_produk.slug, tbl_produk.alamat, tbl_produk.foto, tbl_produk.deskripsi, tbl_produk.view, tbl_produk.created_at, tbl_toko.nama, tbl_toko.logo, tbl_toko.slug AS slug_toko FROM tbl_produk INNER JOIN tbl_toko ON tbl_produk.id_toko=tbl_toko.id WHERE tbl_toko.slug='$slug' AND tbl_produk.status='Aktif' ORDER BY tbl_produk.view DESC LIMIT 2")->result_array();

            $data['produk_aktif'] = $this->db->query("SELECT tbl_produk.id_toko, tbl_produk.produk, tbl_produk.harga, tbl_produk.satuan_harga, tbl_produk.slug, tbl_produk.alamat, tbl_produk.foto, tbl_produk.deskripsi, tbl_produk.view, tbl_produk.created_at, tbl_toko.nama, tbl_toko.logo, tbl_toko.slug AS slug_toko FROM tbl_produk INNER JOIN tbl_toko ON tbl_produk.id_toko=tbl_toko.id WHERE tbl_toko.slug='$slug' AND tbl_produk.status='Aktif' ORDER BY tbl_produk.id DESC")->result_array();

            $data['populer']   = $this->db->query("SELECT * FROM tbl_tulisan ORDER BY tulisan_views DESC LIMIT 5");
            $data['category']  = $this->db->get('tbl_kategori');
            $this->load->view('depan/toko/show_toko',$data);
        }else{
            show_404();
        }
    }
}