<?php
class Daftar extends CI_Controller{
    function __construct(){
		parent::__construct();
		$this->load->library('recaptcha');
	}

    function index(){
        $data['title'] = "Daftar";
        $data['captcha'] = $this->recaptcha->getWidget();
        $data['script_captcha'] = $this->recaptcha->getScriptTag();
        $this->load->view('v_daftar', $data);
    }

    public function store()
    {
        $recaptcha = $this->input->post('g-recaptcha-response');
        if (!empty($recaptcha)) {
            $response = $this->recaptcha->verifyResponse($recaptcha);
            if (isset($response['success']) and $response['success'] === true) {
                $email = $this->input->post('email');
                $nik = $this->input->post('nik');
                $cek_email = $this->db->query("SELECT * FROM tbl_penduduk WHERE email='$email' OR nik='$nik'");
                if(!$cek_email->num_rows() > 0){
                    $password = $this->input->post('password');
                    $password2 = $this->input->post('password2');
                    if($password == $password2){
                        $data = [
                            'nik' => $this->input->post('nik'),
                            'nama' => $this->input->post('nama'),
                            'email' => $this->input->post('email'),
                            'password' => md5($this->input->post('password2')),
                            'jenis_kelamin' => $this->input->post('jenis_kelamin'),
                            'alamat' => $this->input->post('alamat'),
                            'no_tlp' => $this->input->post('no_tlp'),
                        ];
                        $insert = $this->db->insert("tbl_penduduk", $data);
                        if($insert){
                            echo $this->session->set_flashdata('msg_success','<div class="alert alert-info" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Pendaftaran berhasil, silahkan login sekarang</div>');
                            redirect('login');
                        }else{
                            echo $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Gagal mendaftar</div>');
                            redirect('/daftar');
                        }
                    }else{
                        echo $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Password Tidak sama</div>');
                        redirect('/daftar');
                    }
                }else{
                    echo $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Email atau Nik sudah digunakkan</div>');
                    redirect('/daftar');
                }
            }
        }else{
            echo $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Captcha harus di isi!</div>');
            redirect('/daftar');
        }

    }
}