<?php
class Produk extends CI_Controller{
    function __construct(){
		parent::__construct();
		$this->load->model('m_produk');
	}

    public function index()
    {
        $data['title'] = "Produk";
        $data['category']=$this->db->get('tbl_kategori');
		$data['populer']=$this->db->query("SELECT * FROM tbl_tulisan ORDER BY tulisan_views DESC LIMIT 5");

        $jum=$this->m_produk->produk();
        $page=$this->uri->segment(3);
        if(!$page):
            $offset = 0;
        else:
            $offset = $page;
        endif;
        $limit=5;
        $config['base_url'] = base_url() . 'produk/index/';
        $config['total_rows'] = $jum->num_rows();
        $config['per_page'] = $limit;
        $config['uri_segment'] = 3;
        //Tambahan untuk styling
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
        $config['first_link'] = 'Awal';
        $config['last_link'] = 'Akhir';
        $config['next_link'] = 'Next >>';
        $config['prev_link'] = '<< Prev';
        $this->pagination->initialize($config);
        $data['page'] =$this->pagination->create_links();
        $data['data']=$this->m_produk->produk_perpage($offset,$limit);

        $this->load->view('depan/produk/index', $data);
    }
    public function detail($slugs){
        $slug   = htmlspecialchars($slugs,ENT_QUOTES);
		$data['title'] = "Produk";
        $query = $this->db->query("SELECT slug FROM tbl_produk WHERE slug='$slug' AND status='Aktif'");
        if($query->num_rows() > 0){
            $this->db->query("UPDATE tbl_produk SET view=view+1 WHERE slug='$slug'");

            $data['produk'] = $this->db->query("SELECT tbl_produk.id_toko, tbl_produk.id, tbl_produk.produk, tbl_produk.harga, tbl_produk.satuan_harga, tbl_produk.alamat, tbl_produk.foto, tbl_produk.deskripsi, tbl_produk.view, tbl_produk.created_at, tbl_toko.nama, tbl_toko.logo, tbl_toko.slug AS slug_toko, tbl_toko.no_telepon, tbl_toko.no_wa, tbl_toko.facebook, tbl_toko.instagram FROM tbl_produk INNER JOIN tbl_toko ON tbl_produk.id_toko=tbl_toko.id WHERE tbl_produk.slug='$slug' AND tbl_produk.status='Aktif'")->row_array();

            $data['produk_aktif'] = $this->db->query("SELECT tbl_produk.id_toko, tbl_produk.produk, tbl_produk.harga, tbl_produk.satuan_harga, tbl_produk.slug, tbl_produk.alamat, tbl_produk.foto, tbl_produk.deskripsi, tbl_produk.view, tbl_produk.created_at, tbl_toko.nama, tbl_toko.logo, tbl_toko.slug AS slug_toko, tbl_toko.no_telepon, tbl_toko.no_wa, tbl_toko.facebook, tbl_toko.instagram FROM tbl_produk INNER JOIN tbl_toko ON tbl_produk.id_toko=tbl_toko.id WHERE tbl_produk.status='Aktif' ORDER BY tbl_produk.id DESC LIMIT 5")->result_array();

            $data['populer']   = $this->db->query("SELECT * FROM tbl_tulisan ORDER BY tulisan_views DESC LIMIT 5");

            $data['category']  = $this->db->get('tbl_kategori');

            $this->load->view('depan/produk/show_produk',$data);

        }else{
            show_404();
        }
	}
}