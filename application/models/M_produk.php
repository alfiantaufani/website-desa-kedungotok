<?php
class M_produk extends CI_Model{
    function produk(){
		$hsl=$this->db->query("SELECT tbl_produk.*,DATE_FORMAT(created_at,'%d/%m/%Y') AS tanggal FROM tbl_produk ORDER BY id DESC");
		return $hsl;
	}

    function produk_perpage($offset,$limit){
		$hsl=$this->db->query("SELECT tbl_produk.*,DATE_FORMAT(created_at,'%d/%m/%Y') AS tanggal, tbl_produk.id_toko, tbl_produk.produk, tbl_produk.harga, tbl_produk.satuan_harga, tbl_produk.slug, tbl_produk.alamat, tbl_produk.foto, tbl_produk.deskripsi, tbl_produk.view, tbl_produk.created_at, tbl_toko.nama, tbl_toko.logo, tbl_toko.slug AS slug_toko, tbl_toko.no_telepon, tbl_toko.no_wa, tbl_toko.facebook, tbl_toko.instagram FROM tbl_produk INNER JOIN tbl_toko ON tbl_produk.id_toko=tbl_toko.id WHERE tbl_produk.status='Aktif' ORDER BY tbl_produk.id DESC limit $offset,$limit");
		return $hsl;
	}
}